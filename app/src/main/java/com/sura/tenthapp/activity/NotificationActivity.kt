package com.sura.tenthapp.activity

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sura.tenthapp.R
import com.sura.tenthapp.adapter.NotificationAdapter
import com.sura.tenthapp.utils.LoaderDialog
import com.sura.tenthapp.utils.SharedHelperModel
import com.sura.tenthapp.utils.UrlHelper
import com.sura.tenthapp.utils.volley.IResult
import com.sura.tenthapp.utils.volley.VolleyService
import kotlinx.android.synthetic.main.activity_notification.*
import org.json.JSONObject
import java.util.HashMap
import java.util.logging.Logger

class NotificationActivity : AppCompatActivity() {

    private var loading: Boolean = false
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var notificationAdapter: NotificationAdapter
    var currentPage: Int = 1
    var totalPage: Int = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)
        backButton.setOnClickListener { onBackPressed() }
        notificationAdapter = NotificationAdapter(this)
        layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        notificationRecyclerView.layoutManager = layoutManager
        notificationRecyclerView.adapter = notificationAdapter
        getNotificationList(currentPage)

        setRecyclerListener()
    }

    private fun setRecyclerListener() {
        notificationRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = layoutManager.childCount
                val totalItemCount = layoutManager.itemCount
                val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
                if (!loading ) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                        currentPage+=1
                        if(totalPage>=currentPage)
                            getNotificationList(currentPage)
                    }
                }
            }
        })
    }

    private fun getNotificationList(pagenumber: Int) {
        loading = true
        val loaderDialog = LoaderDialog.showLoader(this)

        val mResultCallback = object : IResult {

            override fun notifySuccess(requestType: String, response: JSONObject) {
                Log.d("Notification", ""+response)
                loading = false
                loaderDialog.dismiss()
                if (response.optString("error") == "false") {
                    totalPage = response.optInt("total_page")
                    notificationAdapter.setData(response.optJSONArray("list_notification"))
                } else {
                    Toast.makeText(applicationContext, response.optString("error_message"), Toast.LENGTH_SHORT).show()
                }
            }

            override fun notifySuccessString(requestType: String, response: String) {

            }

            override fun notifyError(requestType: String, error: String) {
                loading = false
                loaderDialog.dismiss()
                if (!error.equals("Parse Error.. Please Try Again..", ignoreCase = true)) {
                    retryDialog(error)
                }
            }
        }


        val mVolleyService = VolleyService(mResultCallback, this)
        val headers = HashMap<String, String>()
        headers.put("authorization", SharedHelperModel(this).accessToken)


        val body = HashMap<String, String>()
        body.put("pageno", pagenumber.toString())

        mVolleyService.postDataVolley("Postcall", UrlHelper.list_notification_v2, body, headers)
    }

    fun retryDialog(error: String) {
        try {
            val retryDialog = Dialog(this)
            retryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            retryDialog.setContentView(R.layout.no_internetconnection_dialog)
            retryDialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
            retryDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            retryDialog.window!!.setGravity(Gravity.CENTER)
            val retryButton = retryDialog.findViewById(R.id.retryButton) as Button
            val retryDialogText = retryDialog.findViewById(R.id.retryDialogText) as TextView
            retryDialogText.text = error
            retryButton.setOnClickListener(View.OnClickListener {
                retryDialog.dismiss()

                getNotificationList(currentPage)

            })
            retryDialog.show()

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


}

