package com.sura.tenthapp.activity

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.facebook.share.Share
import com.sura.tenthapp.R
import com.sura.tenthapp.modules.pdf_files.PdfViewer
import com.sura.tenthapp.utils.LoaderDialog
import com.sura.tenthapp.utils.SharedHelperModel
import com.sura.tenthapp.utils.UrlHelper
import com.sura.tenthapp.utils.volley.IResult
import com.sura.tenthapp.utils.volley.VolleyService
import kotlinx.android.synthetic.main.activity_notification_detail.*
import org.json.JSONObject
import java.util.HashMap

class NotificationDetailActivity : AppCompatActivity() {

    private var id: String=""

    private var link: String=""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification_detail)
        id=intent.getStringExtra("id")
//        description.setMovementMethod(LinkMovementMethod.getInstance())
        backButton.setOnClickListener { onBackPressed() }
        openLink.setOnClickListener {
            if(getExtention(link).equals("pdf")){
                val intent = Intent(this, PdfViewer::class.java)
                intent.putExtra("link", link)
                intent.putExtra("id", "")
                startActivity(intent)
            }else{
                val intent = Intent(this, ImageViewer::class.java)
                intent.putExtra("link", link)
                startActivity(intent)
            }
        }
        getDescription(id)
    }

    private fun getDescription(id: String) {
        val loaderDialog = LoaderDialog.showLoader(this)

        val mResultCallback = object : IResult {

            override fun notifySuccess(requestType: String, response: JSONObject) {
                Log.d("Notification", ""+response)
                loaderDialog.dismiss()
                if (response.optString("error") == "false") {
                    description.text = response.optString("message")
                    if(response.has("url")){
                        link = response.optString("url")
                        if(link.equals("null")){
                            openLink.visibility=View.GONE
                        }else{
                            if(getExtention(link).equals("pdf")){
                                openLink.text = resources.getText(R.string.viewPdf)
                                openLink.visibility=View.VISIBLE

                            }else{
                                Glide.with(this@NotificationDetailActivity).load(link).error(R.drawable.logo).into(image)
                                openLink.text = resources.getText(R.string.viewImage)
                                openLink.visibility=View.GONE
                            }
                        }

                    }
                    else{
                        openLink.visibility=View.GONE
                    }

                } else {
                    Toast.makeText(applicationContext, response.optString("error_message"), Toast.LENGTH_SHORT).show()
                }
            }

            override fun notifySuccessString(requestType: String, response: String) {

            }

            override fun notifyError(requestType: String, error: String) {
                loaderDialog.dismiss()
                if (!error.equals("Parse Error.. Please Try Again..", ignoreCase = true)) {
                    retryDialog(error)
                }
            }
        }


        val mVolleyService = VolleyService(mResultCallback, this)
        val headers = HashMap<String, String>()
        headers.put("authorization", SharedHelperModel(this).accessToken)

        val body = HashMap<String, String>()
        body.put("id", id)

        mVolleyService.postDataVolley("Postcall", UrlHelper.notification_detail_v2, body, headers)
    }

    fun retryDialog(error: String) {
        try {
            val retryDialog = Dialog(this)
            retryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            retryDialog.setContentView(R.layout.no_internetconnection_dialog)
            retryDialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
            retryDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            retryDialog.window!!.setGravity(Gravity.CENTER)
            val retryButton = retryDialog.findViewById(R.id.retryButton) as Button
            val retryDialogText = retryDialog.findViewById(R.id.retryDialogText) as TextView
            retryDialogText.text = error
            retryButton.setOnClickListener(View.OnClickListener {
                retryDialog.dismiss()

                getDescription(id)

            })
            retryDialog.show()

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun getExtention(url: String): String {
        val filenameArray = url.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        return filenameArray[filenameArray.size - 1]
    }



}

