package com.sura.tenthapp.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.sura.tenthapp.R;
import com.sura.tenthapp.activity.ReadingActivity;

public class ChapterMarkAdapter extends RecyclerView.Adapter<ChapterMarkAdapter.ViewHolder>{

    private Context context;
    public ChapterMarkAdapter(Context chapter) {
        this.context=chapter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_mark_recyclerview,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 6;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout questionButton;
        public ViewHolder(View itemView) {
            super(itemView);
            questionButton=itemView.findViewById(R.id.questionButton);
            questionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, ReadingActivity.class);
                    context.startActivity(intent);
                }
            });
        }
    }
}