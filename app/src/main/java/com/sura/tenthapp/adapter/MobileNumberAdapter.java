package com.sura.tenthapp.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sura.tenthapp.R;
import com.sura.tenthapp.modules.login_register.Enter_Otp;
import com.sura.tenthapp.modules.login_register.MobileNumberLogin;
import com.sura.tenthapp.utils.DatabaseHelper;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MobileNumberAdapter extends RecyclerView.Adapter<MobileNumberAdapter.ViewHolder> {

    private static final String TAG = MobileNumberAdapter.class.getSimpleName();
    public Context context;
    public JSONArray jsonArray;

    public MobileNumberAdapter(Context context, JSONArray jsonArray) {
        this.context=context;
        this.jsonArray=jsonArray;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.mobile_number_recyclerview, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mobileNumberText.setText(jsonArray.optJSONObject(position).optString("mobile"));
    }

    @Override
    public int getItemCount() {
       return jsonArray.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mobileNumberText;
        public RelativeLayout deleteNumberButton;
        public RelativeLayout mobileNumberLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            mobileNumberText=itemView.findViewById(R.id.mobileNumberText);
            deleteNumberButton=itemView.findViewById(R.id.deleteNumberButton);
            mobileNumberLayout=itemView.findViewById(R.id.mobileNumberLayout);

            mobileNumberLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int pos=getAdapterPosition();
                    volleyResponse(jsonArray.optJSONObject(pos).optString("mobile"));

                }
            });

            deleteNumberButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int pos=getAdapterPosition();

                    DatabaseHelper myDb = new DatabaseHelper(context);

                    int isDeleted = myDb.deleteData(jsonArray.optJSONObject(pos).optString("id"));
                    Log.e(TAG, "isDeleted: "+isDeleted );

                    Cursor res = myDb.getAllData();

                    if(res.getCount() == 0) {
                        // show message
                        MobileNumberLogin.mobileNumberDialog.dismiss();

                    }else{

                        JSONArray jsonArrayNotify=new JSONArray();

                        while (res.moveToNext()) {
                            int index = res.getColumnIndex("ID");
                            int index2 = res.getColumnIndex("MOBILE");
                            String id = res.getString(index);
                            String mobile = res.getString(index2);
                            JSONObject jsonObject=new JSONObject();
                            try {
                                jsonObject.put("id",id);
                                jsonObject.put("mobile",mobile);
                                jsonArrayNotify.put(jsonObject);
                            } catch (JSONException e) {

                            }

                        }

                        jsonArray=jsonArrayNotify;
                        notifyDataSetChanged();



                    }




                }
            });
        }
    }

    private void volleyResponse(final String mobile) {

        String version="";

        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
            Log.e(TAG, "getVersionCode: "+version );
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        new SharedHelperModel(context).setMobileNumber(mobile);

        final Dialog loaderDialog = LoaderDialog.showLoader(context);

        IResult mResultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                if(response.optString("error").equals("true")){
                    Toast.makeText(context, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                }else{

                    SharedHelperModel sharedHelperModel=new SharedHelperModel(context);

                    sharedHelperModel.setOtp(response.optString("otp"));
                    sharedHelperModel.setNewUser(response.optString("error_message"));
                    Intent i=new Intent(context,Enter_Otp.class);
                    i.putExtra("mobileNumber",mobile);
                    context.startActivity(i);

                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType,String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback,context);

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("mobile_no",mobile);
        body.put("version",version);
        body.put("resend","0");
        Log.e(TAG, "volleyResponse: "+body );
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("authorization","");
        mVolleyService.postDataVolley("POSTCALL", UrlHelper.getOtp,body,header);
    }

    private void getVersionCode() {

    }


}