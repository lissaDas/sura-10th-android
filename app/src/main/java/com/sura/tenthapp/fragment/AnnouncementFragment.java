package com.sura.tenthapp.fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sura.tenthapp.R;

public class AnnouncementFragment extends Fragment {

    private static final String descriptionFrag = "description";
    private static final String titleFrag = "title";
    private static final String linkFrag = "link";
    private static final String typeFrag = "type";



    public static AnnouncementFragment newInstance(String title, String descri,String link,String type) {
        AnnouncementFragment fragment = new AnnouncementFragment();
        Bundle args = new Bundle();
        args.putString(titleFrag, title);
        args.putString(descriptionFrag,descri);
        args.putString(linkFrag,link);
        args.putString(typeFrag,type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.announcement_fragment, container, false);
        TextView titleText = rootView.findViewById(R.id.titleText);
        TextView descriptionText = rootView.findViewById(R.id.descriptionText);
        descriptionText.setText(getArguments().getString(descriptionFrag));
        titleText.setText(getArguments().getString(titleFrag));
        ImageView image = rootView.findViewById(R.id.pagerImage);
        if(getArguments().getString(typeFrag).toLowerCase().equalsIgnoreCase("independence")){
            image.setImageDrawable(getResources().getDrawable(R.drawable.independence));
        }else{
            Glide.with(getActivity()).load(getArguments().getString(linkFrag)).placeholder(R.drawable.discount).into(image);
        }

        return rootView;
    }

}