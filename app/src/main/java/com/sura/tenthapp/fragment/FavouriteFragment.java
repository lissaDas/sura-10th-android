package com.sura.tenthapp.fragment;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sura.tenthapp.R;
import com.sura.tenthapp.modules.offline.OfflineAdapter;
import com.sura.tenthapp.utils.Database;
import com.sura.tenthapp.utils.KeyDetailsDialog;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import static com.sura.tenthapp.utils.Utils.referral_concept_enabled;

public class FavouriteFragment extends Fragment {

    private static final String TAG = "FavouriteFragment";
    private RecyclerView favouriteRecyclerView;
    private RecyclerView.Adapter favouriteAdapter;
    private RelativeLayout noFavFoundLayout;
    private TextView noFavText, keysText;
    private Database mHelper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.favourite_fragment, container, false);
        try {
            initViews(view);
//            volleyResponse();
            getFromDb();
            keyDialogListener(view);
            LinearLayout keyDetails=view.findViewById(R.id.keyDetails);
            if(referral_concept_enabled){

                if(new SharedHelperModel(getActivity()).getIsOfferApplied().equalsIgnoreCase("true")){
                    keyDetails.setVisibility(View.GONE);
                }else if(new SharedHelperModel(getActivity()).getIsOfferApplied().equalsIgnoreCase("false")){
                    keyDetails.setVisibility(View.VISIBLE);
                }
            }else{
                keyDetails.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "onCreateView: " + e.getMessage());
        }

        return view;
    }

    private void getFromDb() {
        JSONArray jsonArray=mHelper.selectFromDatabase();
        Boolean isOffline=true;
        if (jsonArray.length()!=0) {
            setFavouriteAdapter(jsonArray,isOffline);
            noFavFoundLayout.setVisibility(View.GONE);
        } else {
            noFavFoundLayout.setVisibility(View.VISIBLE);
            noFavText.setText(getActivity().getResources().getString(R.string.noFav));
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        keysText.setText(new SharedHelperModel(getActivity()).getKeysCount() + " Keys");
//        volleyResponse();
    }

    public void retryDialog(final String error) {
        try {
            final Dialog retryDialog = new Dialog(getActivity());
            retryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            retryDialog.setContentView(R.layout.no_internetconnection_dialog);
            retryDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            retryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            retryDialog.getWindow().setGravity(Gravity.CENTER);
            Button retryButton = retryDialog.findViewById(R.id.retryButton);
            TextView retryDialogText=retryDialog.findViewById(R.id.retryDialogText);
            retryDialogText.setText(error);
            retryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    retryDialog.dismiss();
                    volleyResponse();
                }
            });
            retryDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void volleyResponse() {

        final Dialog loaderDialog = LoaderDialog.showLoader(getActivity());

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                if (response.optString("error").equals("false")) {
                    if (response.optString("error_message").equals("success")) {
                        setFavouriteAdapter(response.optJSONArray("list_chapter_content"),false);
                        noFavFoundLayout.setVisibility(View.GONE);
                    } else {
                        noFavFoundLayout.setVisibility(View.VISIBLE);
                        noFavText.setText(getActivity().getResources().getString(R.string.noFav));
                    }
                } else {
                    Toast.makeText(getActivity(), response.optString("error_message"), Toast.LENGTH_SHORT).show();
                    noFavFoundLayout.setVisibility(View.VISIBLE);
                    noFavText.setText(getActivity().getResources().getString(R.string.tryAgain));
                }


            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
                if(!error.equalsIgnoreCase("Parse Error.. Please Try Again..")){
                    retryDialog(error);
                }
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, getActivity());

        SharedHelperModel sharedHelperModel = new SharedHelperModel(getActivity());
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        mVolleyService.getDataVolley("GETCALL", UrlHelper.getFavourite, headers);
    }

    private void keyDialogListener(View view) {
        LinearLayout keyDetails = view.findViewById(R.id.keyDetails);
        keyDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyDetailsDialog.showDialog(getActivity());
            }
        });
    }


    private void setFavouriteAdapter(JSONArray response, Boolean isOffline) {
        favouriteAdapter = new OfflineAdapter(getActivity(), response, isOffline);
        favouriteRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        favouriteRecyclerView.setAdapter(favouriteAdapter);
    }

    private void initViews(View view) {
        favouriteRecyclerView = view.findViewById(R.id.favouriteRecyclerView);
        noFavFoundLayout = view.findViewById(R.id.noFavFoundLayout);
        noFavText = view.findViewById(R.id.noFavText);
        keysText = view.findViewById(R.id.keysText);
        mHelper = new Database(getActivity());
    }
}