package com.sura.tenthapp.fragment;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sura.tenthapp.R;
import com.sura.tenthapp.modules.pdf_files.FilesAdapter;
import com.sura.tenthapp.utils.KeyDetailsDialog;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.sura.tenthapp.utils.Utils.referral_concept_enabled;

public class FilesFragment extends Fragment {

    private static final String TAG = "FilesFragment";
    private RecyclerView filesRecyclerView;
    private RecyclerView.Adapter filesAdapter;
    private TextView keysText;
    private SwipeRefreshLayout swipeRefresh;
    private ImageView downArrow;
    private LinearLayoutManager linearLayoutManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.files_fragment, container, false);

        initViews(view);
        swipeRefreshLayout();
        keyDialogListener(view);

        LinearLayout keyDetails=view.findViewById(R.id.keyDetails);
        if(referral_concept_enabled){

            if(new SharedHelperModel(getActivity()).getIsOfferApplied().equalsIgnoreCase("true")){
                keyDetails.setVisibility(View.GONE);
            }else if(new SharedHelperModel(getActivity()).getIsOfferApplied().equalsIgnoreCase("false")){
                keyDetails.setVisibility(View.VISIBLE);
            }
        }else{
            keyDetails.setVisibility(View.GONE);
        }

        if(new SharedHelperModel(getActivity()).getBooleanCheckFiles().equalsIgnoreCase("1")){
            String response=new SharedHelperModel(getActivity()).getFilesContent();
            try {
                JSONObject jsonArray=new JSONObject(response);
                if (jsonArray.optString("error").equals("false")) {
                    setFilesAdapter(jsonArray.optJSONArray("list_pdf"));
                } else {
                    Toast.makeText(getActivity(), jsonArray.optString("error_message"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {

            }
        }else{
            try {
                volleyResponse();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "onCreateView: " + e.getMessage());
            }
        }


        return view;
    }

    private void swipeRefreshLayout() {
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    volleyResponse();
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "onCreateView: " + e.getMessage());
                }
                swipeRefresh.setRefreshing(false);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        keysText.setText(new SharedHelperModel(getActivity()).getKeysCount() + " Keys");
    }

    public void retryDialog(final String error) {
        try {
            final Dialog retryDialog = new Dialog(getActivity());
            retryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            retryDialog.setContentView(R.layout.no_internetconnection_dialog);
            retryDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            retryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            retryDialog.getWindow().setGravity(Gravity.CENTER);
            Button retryButton = retryDialog.findViewById(R.id.retryButton);
            TextView retryDialogText=retryDialog.findViewById(R.id.retryDialogText);
            retryDialogText.setText(error);
            retryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    retryDialog.dismiss();
                    volleyResponse();
                }
            });
            retryDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void volleyResponse() {

        final Dialog loaderDialog = LoaderDialog.showLoader(getActivity());
        final SharedHelperModel sharedHelperModel = new SharedHelperModel(getActivity());

        IResult mResultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();

                if (response.optString("error").equals("false")) {
                    setFilesAdapter(response.optJSONArray("list_pdf"));
                    sharedHelperModel.setBooleanCheckFiles("1");
                    sharedHelperModel.setFilesContent(response.toString());
                } else {
                    Toast.makeText(getActivity(), response.optString("error_message"), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
                if(!error.equalsIgnoreCase("Parse Error.. Please Try Again..")){
                    retryDialog(error);
                }
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, getActivity());

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        mVolleyService.getDataVolley("GETCALL", UrlHelper.getFiles, headers);
    }

    private void keyDialogListener(View view) {
        LinearLayout keyDetails = view.findViewById(R.id.keyDetails);
        keyDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyDetailsDialog.showDialog(getActivity());
            }
        });
    }


    private void setFilesAdapter(JSONArray response) {
        linearLayoutManager=new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false);

        filesAdapter = new FilesAdapter(getActivity(), response);
        filesRecyclerView.setLayoutManager(linearLayoutManager);
        filesRecyclerView.setNestedScrollingEnabled(true);
//        filesRecyclerView.addOnScrollListener(new HideShowScrollListener() {
//            @Override
//            public void onHide() {
//                HomeActivity.bottomLayout.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onShow() {
//                HomeActivity.bottomLayout.setVisibility(View.VISIBLE);
//
//            }
//        });
        filesRecyclerView.setAdapter(filesAdapter);

        filesRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int total = linearLayoutManager.getItemCount();
                int firstVisibleItemCount = linearLayoutManager.findFirstVisibleItemPosition();
                int lastVisibleItemCount = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                Log.e(TAG, "onScrolled: "+lastVisibleItemCount );
                if(lastVisibleItemCount==total-1){
                    downArrow.setVisibility(View.GONE);
                }else{
                    downArrow.setVisibility(View.VISIBLE);

                }
            }
        });

    }

    private void initViews(View view) {
        filesRecyclerView = view.findViewById(R.id.filesRecyclerView);
        keysText = view.findViewById(R.id.keysText);
        swipeRefresh = view.findViewById(R.id.swipeRefresh);
        downArrow = view.findViewById(R.id.downArrow);
    }

}