package com.sura.tenthapp.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sura.tenthapp.R;

public class PagerFragment extends Fragment {

    private static final String descriptionFrag = "description";
    private static final String titleFrag = "title";
    private static final String imageUrlFrag = "imageUrl";
    private static final String linkFrag = "link";


    public static PagerFragment newInstance(String title,String descri,String imageUrl,String link) {
        PagerFragment fragment = new PagerFragment();
        Bundle args = new Bundle();
        args.putString(titleFrag, title);
        args.putString(descriptionFrag,descri);
        args.putString(imageUrlFrag,imageUrl);
        args.putString(linkFrag,link);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.pager_fragment, container, false);
        ImageView image = rootView.findViewById(R.id.pagerImage);
        TextView titleText = rootView.findViewById(R.id.titleText);
        TextView descriptionText = rootView.findViewById(R.id.descriptionText);
        Button getButton = rootView.findViewById(R.id.getButton);

        descriptionText.setText(getArguments().getString(descriptionFrag));
        titleText.setText(getArguments().getString(titleFrag));
        Glide.with(getActivity()).load(getArguments().getString(imageUrlFrag)).placeholder(R.drawable.logo).into(image);
        getButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent viewIntent = new Intent(Intent.ACTION_VIEW,Uri.parse(getArguments().getString(linkFrag)));
//                    if (viewIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivity(viewIntent);
//                    }
                }catch (Exception e){
                    e.printStackTrace();
                    Log.e("AdsFragment", "onClick: "+e.getMessage() );
                }

            }
        });
        return rootView;
    }

}