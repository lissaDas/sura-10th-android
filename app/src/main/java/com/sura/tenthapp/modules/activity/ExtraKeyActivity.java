package com.sura.tenthapp.modules.activity;

import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.sura.tenthapp.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ExtraKeyActivity extends AppCompatActivity {

    private Button continueStudyButton;
    private FirebaseAnalytics mFirebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.extra_key);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);
        continueStudyButton = findViewById(R.id.continueStudyButton);

        continueStudyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = getIntent();
//                String from = intent.getStringExtra("from");
//                if (from.equalsIgnoreCase("splash")) {
//                    Intent i = new Intent(ExtraKeyActivity.this, HomeActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(i);
//                }
//                else{
                    ExtraKeyActivity.super.onBackPressed();
//                }
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
