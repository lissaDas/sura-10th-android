package com.sura.tenthapp.modules.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Handler;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.applinks.AppLinkData;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.sura.tenthapp.R;
import com.sura.tenthapp.activity.NotificationActivity;
import com.sura.tenthapp.modules.doubts.answer_lists.AnswersList;
import com.sura.tenthapp.modules.login_register.GetStarted;
import com.sura.tenthapp.activity.HomeActivity;
import com.sura.tenthapp.utils.AppSignatureHashHelper;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.language.BaseActivity;
import com.sura.tenthapp.utils.language.LocaleUtils;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import io.fabric.sdk.android.Fabric;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SplashScreen extends BaseActivity {


    private static final String TAG = SplashScreen.class.getSimpleName();

    private Intent intent;
    private FirebaseAnalytics mFirebaseAnalytics;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash_screen);

        AppLinkData.fetchDeferredAppLinkData(this,
                new AppLinkData.CompletionHandler() {
                    @Override
                    public void onDeferredAppLinkDataFetched(AppLinkData appLinkData) {
                        // Process app link data
                        Log.e(TAG, "onDeferredAppLink: " + appLinkData);
                    }
                }
        );

        AppSignatureHashHelper appSignatureHashHelper = new AppSignatureHashHelper(this);

        // This code requires one time to get Hash keys do comment and share key
        Log.e(TAG, "HashKey: " + appSignatureHashHelper.getAppSignatures().get(0));



        try {
            PackageInfo info = this.getPackageManager().getPackageInfo(
                    "com.sura.tenthapp",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());

//                new SharedHelperModel(this).setDeviceToken(Base64.encodeToString(md.digest(), Base64.DEFAULT));
                Log.d("KeyHash", "KeyHash:" + Base64.encodeToString(md.digest(), Base64.DEFAULT));

            }
        } catch (PackageManager.NameNotFoundException e) {
            e.getMessage();
        } catch (NoSuchAlgorithmException e) {
            e.getMessage();
        }

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                Intent intent = new Intent(SplashScreen.this, CommingSoonActivity.class);
//                startActivity(intent);
//                finish();
//            }
//        }, 3000);

//        volleyResponse();

        afterServerCheck();

    }


    private void volleyResponse() {

        IResult mResultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);


                if (response.optString("error").equalsIgnoreCase("false")) {
                    if (response.optJSONArray("list").optJSONObject(0).optString("apps").equalsIgnoreCase("10")
                            && response.optJSONArray("list").optJSONObject(0).optString("maintanace").equalsIgnoreCase("0")) {
                        afterServerCheck();
                    } else {
                        Intent intent = new Intent(SplashScreen.this, Maintenance.class);
                        intent.putExtra("desc1", response.optJSONArray("list").optJSONObject(0).optString("desc1"));
                        intent.putExtra("desc2", response.optJSONArray("list").optJSONObject(0).optString("desc2"));
                        startActivity(intent);
                        finish();
                    }
                }


            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                Toast.makeText(SplashScreen.this, error, Toast.LENGTH_SHORT).show();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);

        SharedHelperModel sharedHelperModel = new SharedHelperModel(this);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", "");

        mVolleyService.getDataVolley("GETCALL", UrlHelper.checkServer, headers);
    }

    private void afterServerCheck() {
        JSONObject jsonObject = new JSONObject();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);

        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                String value = getIntent().getExtras().getString(key);
                try {
                    jsonObject.put(key, value);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (jsonObject.length() != 0) {

                Log.e(TAG, "onCreate: " + jsonObject.toString());
                if (jsonObject.optString("key").equals("pdf")) {
                    intent = new Intent(this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra("pdf", "1");
                    intent.putExtra("from", "splash");
                    startActivity(intent);
                } else if (jsonObject.optString("key").equals("doubts")) {

                    intent = new Intent(this, AnswersList.class);
                    intent.putExtra("question_id", jsonObject.optString("question_id"));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else if (jsonObject.optString("key").equals("referral")) {

                    new SharedHelperModel(this).setKeysCount(jsonObject.optString("keys_no"));
                    intent = new Intent(this, ExtraKeyActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra("from", "splash");
                    startActivity(intent);

                } else {
                    intent = new Intent(this, NotificationActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    intent.putExtra("pdf", "0");
//                    intent.putExtra("from", "splash");
                    startActivity(intent);
                }
            } else {
                SplashScreenActivity();
            }
        } else {
            SplashScreenActivity();
        }
    }


    private void SplashScreenActivity() {

//        String manufacturer = "xiaomi";
//        if(manufacturer.equalsIgnoreCase(android.os.Build.MANUFACTURER)) {
//            //this will open auto start screen where user can enable permission for your app
//            Intent intent = new Intent();
//            intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
//            startActivity(intent);
//        }

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                SharedHelperModel sharedHelperModel = new SharedHelperModel(SplashScreen.this);
                String status = sharedHelperModel.getLoginStatus();
                Log.e("status", "run: " + sharedHelperModel.getLoginStatus());
                if (status.equals("0") || status.equals("")) {
                    Intent intent = new Intent(SplashScreen.this, GetStarted.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else {

                    String lang = new SharedHelperModel(SplashScreen.this).getLang();
                    Log.e("Language", "Lang: " + lang);
                    if (lang.equals("en")) {
                        sharedHelperModel.setLang("en");
                        Intent intent = new Intent(SplashScreen.this, HomeActivity.class);
                        intent.putExtra("pdf", "0");
                        intent.putExtra("from", "splash");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        LocaleUtils.setLocale(new Locale("en"));
                        LocaleUtils.updateConfig(getApplication(), getBaseContext().getResources().getConfiguration());
                    } else if (lang.equals("ta")) {
                        sharedHelperModel.setLang("ta");
                        Intent intent = new Intent(SplashScreen.this, HomeActivity.class);
                        intent.putExtra("pdf", "0");
                        intent.putExtra("from", "splash");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        LocaleUtils.setLocale(new Locale("ta"));
                        LocaleUtils.updateConfig(getApplication(), getBaseContext().getResources().getConfiguration());
                    } else {
                        Intent intent = new Intent(SplashScreen.this, GetStarted.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }
            }
        }, 1000);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
