package com.sura.tenthapp.modules.ask_doubt;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.sura.tenthapp.R;
import com.sura.tenthapp.utils.language.BaseActivity;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AskDoubt extends BaseActivity {

    private static final String TAG = "AskDoubt";
    private RelativeLayout backButton;
    private Intent intent;
    private String subjectId = "", chapterId = "";
    private EditText questionText;
    private Button postButton;
    private RecyclerView otherDoubtsRecyclerview;
    private RecyclerView.Adapter otherDoubtsAdapter;
    public static TextView keysText, noDoubtsText;
    //    private AdView mAdView;
    private FirebaseAnalytics mFirebaseAnalytics;
    private RelativeLayout adLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ask_doubt);

//        mAdView = findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

        /*InMobiSdk.init(this, "482e77618f9b475b8d4701af71217434");
        InMobiSdk.setLogLevel(InMobiSdk.LogLevel.DEBUG);

        InMobiBanner bannerAd = new InMobiBanner(this, PlacementId.YOUR_PLACEMENT_ID);
        RelativeLayout adContainer = findViewById(R.id.ad_container);
        float density = getResources().getDisplayMetrics().density;
        RelativeLayout.LayoutParams bannerLp = new RelativeLayout.LayoutParams((int) (320 * density), (int) (50 * density));
        bannerLp.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
        adContainer.addView(bannerAd, bannerLp);
        bannerAd.load();

        bannerAd.setListener(new InMobiBanner.BannerAdListener() {
            @Override
            public void onAdLoadSucceeded(InMobiBanner inMobiBanner) {
                Log.d(TAG, "onAdLoadSucceeded");
            }

            @Override
            public void onAdLoadFailed(InMobiBanner inMobiBanner,
                                       InMobiAdRequestStatus inMobiAdRequestStatus) {
                Log.d(TAG, "Banner ad failed to load with error: " +
                        inMobiAdRequestStatus.getMessage());
            }

            @Override
            public void onAdDisplayed(InMobiBanner inMobiBanner) {
                Log.d(TAG, "onAdDisplayed");
            }

            @Override
            public void onAdDismissed(InMobiBanner inMobiBanner) {
                Log.d(TAG, "onAdDismissed");
            }

            @Override
            public void onAdInteraction(InMobiBanner inMobiBanner, Map<Object, Object> map) {
                Log.d(TAG, "onAdInteraction");
            }

            @Override
            public void onUserLeftApplication(InMobiBanner inMobiBanner) {
                Log.d(TAG, "onUserLeftApplication");
            }

            @Override
            public void onAdRewardActionCompleted(InMobiBanner inMobiBanner, Map<Object, Object> map) {
                Log.d(TAG, "onAdRewardActionCompleted");
            }
        });*/

        initViews();
        setListeners();
        subjectId = intent.getStringExtra("subjectId");
        chapterId = intent.getStringExtra("chapterId");
        getDoubtsVolleyResponse();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void retryDialog(final Context context, final String from, final String error) {
        final Dialog retryDialog = new Dialog(AskDoubt.this);
        retryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        retryDialog.setContentView(R.layout.no_internetconnection_dialog);
        retryDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        retryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        retryDialog.getWindow().setGravity(Gravity.CENTER);
        Button retryButton = retryDialog.findViewById(R.id.retryButton);
        TextView retryDialogText = retryDialog.findViewById(R.id.retryDialogText);
        retryDialogText.setText(error);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryDialog.dismiss();
                if (from.equals("get")) {
                    getDoubtsVolleyResponse();

                } else if (from.equals("post")) {
                    volleyResponse();
                }

            }
        });
        retryDialog.show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        keysText.setText(new SharedHelperModel(this).getKeysCount() + " Keys");
    }

    private void getDoubtsVolleyResponse() {

        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                if (response.optString("error").equals("true")) {
                    Toast.makeText(AskDoubt.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                } else {

                    if (response.optString("error_message").equals("No list.")) {
                        noDoubtsText.setVisibility(View.VISIBLE);
                    } else {
                        noDoubtsText.setVisibility(View.GONE);
                        setOtherDoubtsAdapter(response.optJSONArray("list_doubts"));
                    }
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
                if (!error.equalsIgnoreCase("Parse Error.. Please Try Again..")) {

                    retryDialog(AskDoubt.this, "get", error);
                }
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(this);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("subject_id", subjectId);
        mVolleyService.postDataVolley("POSTCALL", UrlHelper.getDoubts, body, headers);

    }

    private void setListeners() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Utils.hideSoftKeyboard(AskDoubt.this);
                AskDoubt.super.onBackPressed();
            }
        });
        postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (questionText.getText().toString().trim().length() >= 50) {
                    volleyResponse();
                } else {
                    questionText.setError("Please Enter Minimum 50 Characters");
                }
            }
        });
    }

    private void volleyResponse() {

        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                if (response.optString("error").equals("false")) {
                    Toast.makeText(AskDoubt.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                    questionText.setText("");
                    getDoubtsVolleyResponse();
                } else {
                    Toast.makeText(AskDoubt.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(this);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());
        Log.e(TAG, "volleyResponse : " + sharedHelperModel.getAccessToken());
        HashMap<String, String> body = new HashMap<String, String>();
        body.put("subject_id", subjectId);
        body.put("doubt", questionText.getText().toString().trim());
        body.put("standard", getResources().getString(R.string.standard));
        body.put("chapter_id", chapterId);

        Log.e(TAG, "volleyResponse: " + body);
        mVolleyService.postDataVolley("POSTCALL", UrlHelper.addDoubts, body, headers);
    }

    private void setOtherDoubtsAdapter(JSONArray jsonArray) {
        otherDoubtsAdapter = new OtherDoubtsAdapter(this, jsonArray);
        otherDoubtsRecyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        otherDoubtsRecyclerview.setAdapter(otherDoubtsAdapter);
    }


    private void initViews() {
        backButton = findViewById(R.id.backButton);
        postButton = findViewById(R.id.postButton);
        questionText = findViewById(R.id.questionText);
        keysText = findViewById(R.id.keysText);
        noDoubtsText = findViewById(R.id.noDoubtsText);
        otherDoubtsRecyclerview = findViewById(R.id.otherDoubtsRecyclerview);
        intent = getIntent();
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
