package com.sura.tenthapp.modules.doubts.answer_lists;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sura.tenthapp.R;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AnswersList extends AppCompatActivity {

    private static final String TAG = AnswersList.class.getSimpleName();
    String questionId="";
    private RecyclerView answerPerSubjectRecyclerview;
    private RecyclerView.Adapter answerPerSubjectAdapter;
    private TextView noDoubtsText,questionText;
    private Button answerButton,postButton;
    private LinearLayout answerLayout;
    private EditText answer;
    private RelativeLayout backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.answers_list);

//        AdView mAdView = findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

//        InMobiSdk.init(this, "482e77618f9b475b8d4701af71217434");
//        InMobiSdk.setLogLevel(InMobiSdk.LogLevel.DEBUG);
//
//        InMobiBanner bannerAd = new InMobiBanner(this, PlacementId.YOUR_PLACEMENT_ID);
//        RelativeLayout adContainer = findViewById(R.id.ad_container);
//        float density = getResources().getDisplayMetrics().density;
//        RelativeLayout.LayoutParams bannerLp = new RelativeLayout.LayoutParams((int) (320 * density), (int) (50 * density));
//        bannerLp.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//        adContainer.addView(bannerAd, bannerLp);
//        bannerAd.load();
//
//        bannerAd.setListener(new InMobiBanner.BannerAdListener() {
//            @Override
//            public void onAdLoadSucceeded(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdLoadSucceeded");
//            }
//
//            @Override
//            public void onAdLoadFailed(InMobiBanner inMobiBanner,
//                                       InMobiAdRequestStatus inMobiAdRequestStatus) {
//                Log.d(TAG, "Banner ad failed to load with error: " +
//                        inMobiAdRequestStatus.getMessage());
//            }
//
//            @Override
//            public void onAdDisplayed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDisplayed");
//            }
//
//            @Override
//            public void onAdDismissed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDismissed");
//            }
//
//            @Override
//            public void onAdInteraction(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdInteraction");
//            }
//
//            @Override
//            public void onUserLeftApplication(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onUserLeftApplication");
//            }
//
//            @Override
//            public void onAdRewardActionCompleted(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdRewardActionCompleted");
//            }
//        });

        initViews();

        Intent i=getIntent();

        questionId=i.getStringExtra("question_id");

        setAnswerLayout();

        getDoubtsVolleyResponse();

        setListener();
    }

    private void setAnswerLayout() {

        if(new SharedHelperModel(AnswersList.this).getLoginStudentOrTeacher().equals("teacher")){
            answerButton.setVisibility(View.VISIBLE);
        }else{
            answerButton.setVisibility(View.GONE);

        }

    }

    private void setListener() {
        answerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                answerLayout.setVisibility(View.VISIBLE);
            }
        });
        postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(answer.getText().toString().trim().length()!=0){
                    postAnswer();
                }else {
                    answer.setError(getResources().getString(R.string.fillThefield));
                }
            }
        });
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AnswersList.super.onBackPressed();
            }
        });
    }

    private void postAnswer() {

        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                if (response.optString("error").equals("true")) {
                    Toast.makeText(AnswersList.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                } else {
                    answerLayout.setVisibility(View.GONE);
                    answer.setText("");
                    getDoubtsVolleyResponse();
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
                if(!error.equalsIgnoreCase("Parse Error.. Please Try Again..")){
                    retryDialog(error);
                }
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(this);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("question_id",questionId);
        body.put("answer",answer.getText().toString().trim());
        mVolleyService.postDataVolley("POSTCALL", UrlHelper.postAnswer, body, headers);
    }

    private void initViews() {

        answerPerSubjectRecyclerview=findViewById(R.id.answerPerSubjectRecyclerview);
        noDoubtsText=findViewById(R.id.noDoubtsText);
        questionText=findViewById(R.id.questionText);
        answerButton=findViewById(R.id.answerButton);
        answerLayout=findViewById(R.id.answerLayout);
        postButton=findViewById(R.id.postButton);
        answer=findViewById(R.id.answer);
        backButton=findViewById(R.id.backButton);

    }

    private void getDoubtsVolleyResponse() {

        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                if (response.optString("error").equals("true")) {
                    Toast.makeText(AnswersList.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                } else {

                    if (response.optString("error_message").equals("No list.")) {
                        noDoubtsText.setVisibility(View.VISIBLE);
//                        Toast.makeText(YourDoubts.this, "No Dobts are there", Toast.LENGTH_SHORT).show();
                    } else {
                        noDoubtsText.setVisibility(View.GONE);
                        questionText.setText(response.optJSONObject("question_list").optString("question"));
                        setYourDoubtsAdapter(response.optJSONObject("question_list").optJSONArray("answers"));
                    }
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
                if(!error.equalsIgnoreCase("Parse Error.. Please Try Again..")){
                    retryDialog(error);
                }
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(this);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("question_id",questionId);
        mVolleyService.postDataVolley("POSTCALL", UrlHelper.getAnswersSubject, body, headers);
    }

    private void setYourDoubtsAdapter(JSONArray jsonArray) {
        answerPerSubjectAdapter = new AnswerPerSubjectAdapter(this, jsonArray);
        answerPerSubjectRecyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        answerPerSubjectRecyclerview.setNestedScrollingEnabled(false);
        answerPerSubjectRecyclerview.setHasFixedSize(true);
        answerPerSubjectRecyclerview.setFocusable(false);
        answerPerSubjectRecyclerview.setAdapter(answerPerSubjectAdapter);
    }

    public void retryDialog(final String error) {
        final Dialog retryDialog = new Dialog(AnswersList.this);
        retryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        retryDialog.setContentView(R.layout.no_internetconnection_dialog);
        retryDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        retryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        retryDialog.getWindow().setGravity(Gravity.CENTER);
        Button retryButton=retryDialog.findViewById(R.id.retryButton);
        TextView retryDialogText = retryDialog.findViewById(R.id.retryDialogText);
        retryDialogText.setText(error);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryDialog.dismiss();

                getDoubtsVolleyResponse();

            }
        });
        retryDialog.show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
