package com.sura.tenthapp.modules.doubts.doubt_lists;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sura.tenthapp.R;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DoubtListPerSubject extends AppCompatActivity {

    private static final String TAG = DoubtListPerSubject.class.getSimpleName();
    String subjectId="",from="";
    private RecyclerView doubtPerSubjectRecyclerview;
    private RecyclerView.Adapter doubtPerSubjectAdapter;
    private TextView noDoubtsText;
    private RelativeLayout backButton;
    private EditText searchEdit;
    private JSONArray jsonArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doubt_list_per_subject);

//        AdView mAdView = findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

//        InMobiSdk.init(this, "482e77618f9b475b8d4701af71217434");
//        InMobiSdk.setLogLevel(InMobiSdk.LogLevel.DEBUG);
//
//        InMobiBanner bannerAd = new InMobiBanner(this, PlacementId.YOUR_PLACEMENT_ID);
//        RelativeLayout adContainer = findViewById(R.id.ad_container);
//        float density = getResources().getDisplayMetrics().density;
//        RelativeLayout.LayoutParams bannerLp = new RelativeLayout.LayoutParams((int) (320 * density), (int) (50 * density));
//        bannerLp.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//        adContainer.addView(bannerAd, bannerLp);
//        bannerAd.load();
//
//        bannerAd.setListener(new InMobiBanner.BannerAdListener() {
//            @Override
//            public void onAdLoadSucceeded(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdLoadSucceeded");
//            }
//
//            @Override
//            public void onAdLoadFailed(InMobiBanner inMobiBanner,
//                                       InMobiAdRequestStatus inMobiAdRequestStatus) {
//                Log.d(TAG, "Banner ad failed to load with error: " +
//                        inMobiAdRequestStatus.getMessage());
//            }
//
//            @Override
//            public void onAdDisplayed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDisplayed");
//            }
//
//            @Override
//            public void onAdDismissed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDismissed");
//            }
//
//            @Override
//            public void onAdInteraction(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdInteraction");
//            }
//
//            @Override
//            public void onUserLeftApplication(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onUserLeftApplication");
//            }
//
//            @Override
//            public void onAdRewardActionCompleted(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdRewardActionCompleted");
//            }
//        });
        
        initViews();

        Intent i=getIntent();

        subjectId=i.getStringExtra("subject_id");
        from=i.getStringExtra("from");

        Log.e(TAG, "subjectId: "+subjectId );

        setListeners();

        getDoubtsVolleyResponse();

    }

    private void setListeners() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DoubtListPerSubject.super.onBackPressed();
            }
        });
        searchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void filter(String s) {
        // Log.e(TAG, "filter: "+array );

        try {
            if (jsonArray != null) {
                if (jsonArray.length() > 0) {
                    JSONArray filerArray = new JSONArray();
                    // Log.e(TAG, "json array length: " + jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        String name = jsonArray.optJSONObject(i).optString("question").toLowerCase();
                        //Log.e(TAG, "filter name array: "+name );
                        if (name.contains(s.toLowerCase())) {
                            //Log.e(TAG, "filter name: " + name);
                            JSONObject filterObj = jsonArray.optJSONObject(i);
                            //Log.e(TAG, "filter: " + filterObj);
                            filerArray.put(filterObj);
                        }
                        //Log.e(TAG, "filter array: " + filerArray);
                    }
                    Log.e(TAG, "filter: "+filerArray.length() );
                    doubtPerSubjectAdapter = new DoubtsListPerSubjectAdapter(this, filerArray);
                    doubtPerSubjectRecyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                    doubtPerSubjectRecyclerview.setAdapter(doubtPerSubjectAdapter);
                    doubtPerSubjectAdapter.notifyDataSetChanged();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initViews() {

        doubtPerSubjectRecyclerview=findViewById(R.id.doubtPerSubjectRecyclerview);
        noDoubtsText=findViewById(R.id.noDoubtsText);
        backButton=findViewById(R.id.backButton);
        searchEdit=findViewById(R.id.searchEdit);

    }

    private void getDoubtsVolleyResponse() {

        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                if (response.optString("error").equals("true")) {
                    Toast.makeText(DoubtListPerSubject.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                } else {

                    if (response.optString("error_message").equals("No list.")) {
                        noDoubtsText.setVisibility(View.VISIBLE);
//                        Toast.makeText(YourDoubts.this, "No Dobts are there", Toast.LENGTH_SHORT).show();
                    } else {
                        noDoubtsText.setVisibility(View.GONE);
                        jsonArray=response.optJSONArray("question_list");
                        setYourDoubtsAdapter(response.optJSONArray("question_list"));
                    }
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
                if(!error.equalsIgnoreCase("Parse Error.. Please Try Again..")){
                    retryDialog(error);
                }
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(this);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        if(from.equals("yourDoubts")){
            body.put("type","user");
        }else{
            body.put("subject_id",subjectId);
        }
        mVolleyService.postDataVolley("POSTCALL", UrlHelper.getDoubtSubject, body, headers);
    }

    private void setYourDoubtsAdapter(JSONArray jsonArrayApi) {
        doubtPerSubjectAdapter = new DoubtsListPerSubjectAdapter(this, jsonArrayApi);
        doubtPerSubjectRecyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        doubtPerSubjectRecyclerview.setAdapter(doubtPerSubjectAdapter);
    }



    public void retryDialog(final String error) {
        final Dialog retryDialog = new Dialog(DoubtListPerSubject.this);
        retryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        retryDialog.setContentView(R.layout.no_internetconnection_dialog);
        retryDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        retryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        retryDialog.getWindow().setGravity(Gravity.CENTER);
        Button retryButton=retryDialog.findViewById(R.id.retryButton);
        TextView retryDialogText = retryDialog.findViewById(R.id.retryDialogText);
        retryDialogText.setText(error);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryDialog.dismiss();

                getDoubtsVolleyResponse();

            }
        });
        retryDialog.show();
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
