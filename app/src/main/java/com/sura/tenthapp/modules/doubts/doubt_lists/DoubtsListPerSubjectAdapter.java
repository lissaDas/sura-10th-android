package com.sura.tenthapp.modules.doubts.doubt_lists;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sura.tenthapp.R;
import com.sura.tenthapp.modules.doubts.answer_lists.AnswersList;

import org.json.JSONArray;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DoubtsListPerSubjectAdapter extends RecyclerView.Adapter<DoubtsListPerSubjectAdapter.ViewHolder> {

    private static final String TAG = DoubtsListPerSubjectAdapter.class.getSimpleName();
    public Context context;
    public JSONArray jsonArray;

    public DoubtsListPerSubjectAdapter(Context context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.doubtspersubject_recyclerview, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.answerCountText.setText(jsonArray.optJSONObject(position).optString("answers"));
        holder.questionsText.setText(jsonArray.optJSONObject(position).optString("question"));

        holder.askedOnText.setText("Asked on : "+convertDateFormat(jsonArray.optJSONObject(position).optString("created_at")).substring(0,11));

    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView answerCountText, questionsText,askedOnText;
public LinearLayout questionsSubjectButton;
        public ViewHolder(View itemView) {
            super(itemView);

            answerCountText = itemView.findViewById(R.id.answerCountText);
            questionsText = itemView.findViewById(R.id.questionsText);
            questionsSubjectButton= itemView.findViewById(R.id.questionsSubjectButton);
            askedOnText= itemView.findViewById(R.id.askedOnText);

            questionsSubjectButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, AnswersList.class);
                    intent.putExtra("question_id",jsonArray.optJSONObject(getAdapterPosition()).optString("id"));
                    context.startActivity(intent);
                }
            });

        }
    }

    public String convertDateFormat(String dateFormat) {

        Log.e(TAG, "convertDateFormat: "+dateFormat );
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(dateFormat);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}