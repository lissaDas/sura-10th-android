package com.sura.tenthapp.modules.login_register;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.InputFilter;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.sura.tenthapp.R;
import com.sura.tenthapp.adapter.MobileNumberAdapter;
import com.sura.tenthapp.utils.language.BaseActivity;
import com.sura.tenthapp.utils.DatabaseHelper;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import net.alhazmy13.gota.Gota;
import net.alhazmy13.gota.GotaResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MobileNumberLogin extends BaseActivity implements Gota.OnRequestPermissionsBack,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "MobileNumberLogin";
    Button sendOtpButton;
    EditText mobileNumberEdit;
    SharedHelperModel sharedHelperModel;
    InputFilter filter;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String version = "";
    DatabaseHelper myDb;
    public static Dialog mobileNumberDialog;
private int RC_HINT=2;
    private GoogleApiClient mCredentialsApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile_number);

        initViews();
        getVersionCode();
        setPermission();
        setlisteners();
        mCredentialsApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(MobileNumberLogin.this)
                .enableAutoManage(this, this)
                .addApi(Auth.CREDENTIALS_API)
                .build();
        //showMobileNumbers();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);
        requestHint();

    }

    @SuppressLint("LongLogTag")
    private void requestHint() {
        HintRequest hintRequest = new HintRequest.Builder()
                .setPhoneNumberIdentifierSupported(true)
                .build();

        PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(
                mCredentialsApiClient, hintRequest);

        try {
            startIntentSenderForResult(intent.getIntentSender(),
                    RC_HINT, null, 0, 0, 0);
        } catch (Exception e) {
            Log.e("Error In getting Message", e.getMessage());
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_HINT && resultCode == Activity.RESULT_OK) {

            /*You will receive user selected phone number here if selected and send it to the server for request the otp*/
            assert data != null;
            Credential number=data.getParcelableExtra(Credential.EXTRA_KEY);
            Log.e(TAG, "onActivityResult: "+number.getId().substring(3) );
            mobileNumberEdit.setText(number.getId().substring(3));
            sharedHelperModel.setMobileNumber(mobileNumberEdit.getText().toString());
            volleyResponse();
        }
    }

    private void showMobileNumbers() {

        mobileNumberDialog = new Dialog(this);
        mobileNumberDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mobileNumberDialog.setContentView(R.layout.mobile_number_dialog);
        mobileNumberDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        mobileNumberDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//      loaderDialog.getWindow().setBackgroundDrawable(context.getResources().getDrawable(R.color.transparentWhite));
        RecyclerView recyclerViewMobileNumber=mobileNumberDialog.findViewById(R.id.recyclerViewMobileNumber);
        RelativeLayout closeButton=mobileNumberDialog.findViewById(R.id.closeButton);
        Button enterMobileNumber=mobileNumberDialog.findViewById(R.id.enterMobileNumber);
        final RecyclerView.Adapter mobileNumberAdapter;
        mobileNumberDialog.getWindow().setGravity(Gravity.CENTER);
        mobileNumberDialog.setCanceledOnTouchOutside(false);
        mobileNumberDialog.setCancelable(true);

        enterMobileNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mobileNumberDialog.dismiss();
            }
        });



        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mobileNumberDialog.dismiss();
            }
        });


            Cursor res = myDb.getAllData();
            if(res.getCount() == 0) {
                // show message
                mobileNumberDialog.dismiss();

            }else{

                JSONArray jsonArray=new JSONArray();

                while (res.moveToNext()) {
                    int index = res.getColumnIndex("ID");
                    int index2 = res.getColumnIndex("MOBILE");
                    String id = res.getString(index);
                    String mobile = res.getString(index2);
                    JSONObject jsonObject=new JSONObject();
                    try {
                        jsonObject.put("id",id);
                        jsonObject.put("mobile",mobile);
                        jsonArray.put(jsonObject);
                    } catch (JSONException e) {

                    }
                }
                mobileNumberDialog.show();

                Log.e(TAG, "showMobileNumbers: "+jsonArray );
                mobileNumberAdapter = new MobileNumberAdapter(this, jsonArray);
                recyclerViewMobileNumber.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                recyclerViewMobileNumber.setAdapter(mobileNumberAdapter);
            }
    }

    public void showMessage(String title,String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }


    private void setPermission() {
        new Gota.Builder(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_PHONE_STATE)
                .requestId(1)
                .setListener(MobileNumberLogin.this)
                .check();
    }

    private void setlisteners() {
        sendOtpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mobileNumberEdit.getText().toString().length()==10){

                    sharedHelperModel.setMobileNumber(mobileNumberEdit.getText().toString());
//                    Intent i=new Intent(MobileNumberLogin.this, HomeActivity.class);
//                    i.putExtra("mobileNumber",mobileNumberEdit.getText().toString().trim());
//                    startActivity(i);

                    volleyResponse();
                }
                else{
                    mobileNumberEdit.setError("Enter valid Mobile Number");
                }
            }
        });
    }

    private void getVersionCode() {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
            Log.e(TAG, "getVersionCode: "+version );
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }


    private void volleyResponse() {


        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();

                if(response.optString("error").equals("true")){
                    Toast.makeText(MobileNumberLogin.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                }else{

                    boolean isInserted = myDb.insertData(sharedHelperModel.getMobileNumber() );
                    Log.e(TAG, "isInserted: "+isInserted );

                    sharedHelperModel.setOtp(response.optString("otp"));
                    sharedHelperModel.setNewUser(response.optString("error_message"));
                    Intent i=new Intent(MobileNumberLogin.this,Enter_Otp.class);
                    i.putExtra("mobileNumber",mobileNumberEdit.getText().toString().trim());
                    startActivity(i);

                }


            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType,String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback,this);

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("mobile_no",mobileNumberEdit.getText().toString());
        body.put("version",version);
        body.put("resend","0");
        Log.e(TAG, "volleyResponse: "+body );
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("authorization","");
        mVolleyService.postDataVolley("POSTCALL", UrlHelper.getOtp,body,header);
    }

    private void initViews() {
        mobileNumberEdit=findViewById(R.id.mobileNumberEdit);
        sendOtpButton=findViewById(R.id.sendOtpButton);
        sharedHelperModel=new SharedHelperModel(this);
        myDb = new DatabaseHelper(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onRequestBack(int requestId, @NonNull GotaResponse gotaResponse) {

        if(gotaResponse.isGranted(Manifest.permission.READ_SMS)) {

        }
        if(gotaResponse.isGranted(Manifest.permission.READ_EXTERNAL_STORAGE)) {

        }
        if(gotaResponse.isGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

        }
//        else{
//            Toast.makeText(this, "Please Enable Permission", Toast.LENGTH_SHORT).show();
//            Intent intent = new Intent();
//            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//            Uri uri = Uri.fromParts("package", getPackageName(), null);
//            intent.setData(uri);
//            startActivity(intent);
//        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
