package com.sura.tenthapp.modules.login_register;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.sura.tenthapp.modules.activity.ChooseMedium;
import com.sura.tenthapp.utils.AWS.UploadCallBack;
import com.sura.tenthapp.utils.AWS.UploadImage;
import com.sura.tenthapp.R;
import com.sura.tenthapp.utils.SelectedSubjectArray;
import com.sura.tenthapp.utils.language.BaseActivity;
import com.sura.tenthapp.utils.GetRealPath;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import net.alhazmy13.gota.Gota;
import net.alhazmy13.gota.GotaResponse;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Register extends BaseActivity implements Gota.OnRequestPermissionsBack {

    private static final String TAG = "Register";
    Button registerButton;
    private Spinner selectGender, groupSpinner;
    private EditText nameEdit, schoolEdit, referalCodeEdit, districtEdit, emailEdit;
    private SharedHelperModel sharedHelperModel;
    InputFilter filter;
    private FirebaseAnalytics mFirebaseAnalytics;
    private RelativeLayout imagePickButton;
    private CircleImageView profilePicture;
    private String realPathUrl = "";
    RadioGroup whoAreYouRadioGroup, selectStandardRadioGroup;
    private LinearLayout teacherLayout, groupLayout;
    private JSONArray tenthSubjectJsonArray, twelthSubjectJsonArray;
    String[] tenthGroupArray, twelthGroupArray;
    private RecyclerView setSubjectRecyclerview;
    private SubjectListRegisterAdapter setSubjectAdapter;
    private List<Subjects> subjects;
    JSONArray selectedSubjects;
    JSONArray newSelectedArray;
    int Count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initViews();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);
        //setFilter();
        subjects = new ArrayList<>();
        setListener();
        getResponse();
        String[] catagory_event = new String[]{"Male", "Female"};
        ArrayAdapter<String> catagory_adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, catagory_event);
        selectGender.setAdapter(catagory_adapter);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void setFilter() {

        nameEdit.setFilters(new InputFilter[]{filter});
        schoolEdit.setFilters(new InputFilter[]{filter});
        referalCodeEdit.setFilters(new InputFilter[]{filter});
        districtEdit.setFilters(new InputFilter[]{filter});

        filter = new InputFilter() {
            boolean canEnterSpace = false;

            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {

                if (nameEdit.getText().toString().equals("")) {
                    canEnterSpace = false;
                }
                if (schoolEdit.getText().toString().equals("")) {
                    canEnterSpace = false;
                }
                if (referalCodeEdit.getText().toString().equals("")) {
                    canEnterSpace = false;
                }
                if (districtEdit.getText().toString().equals("")) {
                    canEnterSpace = false;
                }

                StringBuilder builder = new StringBuilder();

                for (int i = start; i < end; i++) {
                    char currentChar = source.charAt(i);
                    if (Character.isLetterOrDigit(currentChar) || currentChar == '_') {
                        builder.append(currentChar);
                        canEnterSpace = true;
                    }
                    if (Character.isWhitespace(currentChar) && canEnterSpace) {
                        builder.append(currentChar);
                    }
                }
                return builder.toString();
            }
        };
    }

    private void setListener() {
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int radioButtonID = whoAreYouRadioGroup.getCheckedRadioButtonId();
                View radioButton = whoAreYouRadioGroup.findViewById(radioButtonID);
                int idx = whoAreYouRadioGroup.indexOfChild(radioButton);

                RadioButton r = (RadioButton) whoAreYouRadioGroup.getChildAt(idx);
                String type = r.getText().toString().toLowerCase();

                if (type.equals("student")) {

                    if (nameEdit.getText().toString().length() != 0) {

                        if (realPathUrl.equalsIgnoreCase("")) {
                            volleyResponse("none", type);
                        } else {
                            uploadDp(type);
                        }

                    } else {
                        nameEdit.setError("Please fill the field");
                    }

                } else if (type.equals("teacher")) {

                    if (nameEdit.getText().toString().length() != 0) {

                        selectedSubjects = new JSONArray();
                        for (int i = 0; i < subjects.size(); i++) {
                            if (subjects.get(i).isSelected()) {
                                JSONObject object = new JSONObject();
                                try {
                                    object.put("subject_id", subjects.get(i).getSubjectId());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                selectedSubjects.put(object);
                            }
                        }

//                        selectedSubjects = new JSONArray();
//                        for (int i = 0; i < newSelectedArray.length(); i++) {
//
//                            JSONObject jsonObject = new JSONObject();
//
//                            if (newSelectedArray.optJSONObject(i).optString("isSelected").equals("true")) {
//                                //Count = Count + 1;
//                                try {
//                                    jsonObject.put("subject_id", newSelectedArray.optJSONObject(i).optString("subject_id"));
//                                    selectedSubjects.put(jsonObject);
//                                } catch (JSONException e) {
//
//                                }
//
//
//                            }
//                        }

                        Log.e(TAG, "selectedSubjects: " + selectedSubjects);

                        if (selectedSubjects.length() == 0) {
                            Toast.makeText(Register.this, "Please Select Subjects", Toast.LENGTH_SHORT).show();
                        } else {

                            if (realPathUrl.equalsIgnoreCase("")) {
                                volleyResponse("none", "teacher");
                            } else {
                                uploadDp("teacher");
                            }

                        }
                    } else {
                        nameEdit.setError("Please fill the field");
                    }


                }


            }
        });

        imagePickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSelectImageClick(view);
            }
        });

        whoAreYouRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton checkedRadioButton = radioGroup.findViewById(i);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked) {
                    Log.e(TAG, "onCheckedChanged: " + checkedRadioButton.getText());

                } else {
                    Log.e(TAG, "onCheckedChanged: " + checkedRadioButton.getText());
                }

                if (checkedRadioButton.getText().toString().toLowerCase().equals("student")) {
                    teacherLayout.setVisibility(View.GONE);
                    groupLayout.setVisibility(View.GONE);
                } else {
//                    getResponse();

                    teacherLayout.setVisibility(View.VISIBLE);
                    groupLayout.setVisibility(View.VISIBLE);


                }
            }
        });

    }

    public void onSelectImageClick(View view) {
        CropImage.startPickImageActivity(this);
    }


    private void getResponse() {

        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                //Check error message on response
                if (response.optString("error").equals("true")) {
                    Toast.makeText(Register.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                } else {
                    tenthSubjectJsonArray = response.optJSONArray("tenth_subject");
                    subjects = new ArrayList<>();

                    for (int i = 0; i < tenthSubjectJsonArray.length(); i++) {
                        Subjects subject = new Subjects(tenthSubjectJsonArray.optJSONObject(i).optString("subject_name"), tenthSubjectJsonArray.optJSONObject(i).optString("subject_id"), false);
                        subjects.add(subject);
                    }
                    setSubjectsAdaper();
//                    groupLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
                Toast.makeText(Register.this, error, Toast.LENGTH_SHORT).show();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);


        HashMap<String, String> header = new HashMap<String, String>();
        header.put("authorization", "");

        mVolleyService.getDataVolley("POSTCALL", UrlHelper.getAllSubjects, header);


    }

    private void setSubjectsAdaper() {
        setSubjectAdapter = new SubjectListRegisterAdapter(this, subjects);
        setSubjectRecyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        setSubjectRecyclerview.setHasFixedSize(true);
        setSubjectRecyclerview.setNestedScrollingEnabled(false);
//        setSubjectRecyclerview.setNestedScrollingEnabled(false);
        setSubjectRecyclerview.setAdapter(setSubjectAdapter);
    }


    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                setPermission();
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                //((ImageButton) findViewById(R.id.quick_start_cropped_image)).setImageURI(result.getUri());
                realPathUrl = GetRealPath.getRealPathFromUri(this, result.getUri());
                Glide.with(this).load(realPathUrl).into(profilePicture);
                Log.d(TAG, "onActivityResult: " + realPathUrl);
                //Toast.makeText(this, "Cropping successful, Sample: " + url, Toast.LENGTH_LONG).show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setMultiTouchEnabled(true)
                .setAspectRatio(10, 10)
                .start(this);
    }

    private void setPermission() {
        new Gota.Builder(this)
                .withPermissions(Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .requestId(1)
                .setListener(Register.this)
                .check();
    }

    @Override
    public void onRequestBack(int requestId, @NonNull GotaResponse gotaResponse) {
        if (gotaResponse.isGranted(Manifest.permission.READ_SMS)) {

        }
//        else{
//            Toast.makeText(this, "Please Enable Permission", Toast.LENGTH_SHORT).show();
//            Intent intent = new Intent();
//            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//            Uri uri = Uri.fromParts("package", getPackageName(), null);
//            intent.setData(uri);
//            startActivity(intent);
//        }
    }

    public void uploadDp(final String standard) {
        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        File displayPicture = new File(realPathUrl);

        UploadImage.uploadFile(Register.this, displayPicture, new UploadCallBack() {
            @Override
            public void result(boolean status, String message) {
                if (status) {
                    Log.e(TAG, "result: " + message);
                    loaderDialog.dismiss();
                    volleyResponse(message, standard);
                } else {
                    loaderDialog.dismiss();
                    Toast.makeText(Register.this, "Failed to Upload Profile Picture", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void volleyResponse(String message, String standard) {


        int radioButtonID = whoAreYouRadioGroup.getCheckedRadioButtonId();
        View radioButton = whoAreYouRadioGroup.findViewById(radioButtonID);
        int idx = whoAreYouRadioGroup.indexOfChild(radioButton);

        RadioButton r = (RadioButton) whoAreYouRadioGroup.getChildAt(idx);
        String type = r.getText().toString();

        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                //Check error message on response
                if (response.optString("error").equals("true")) {
                    Toast.makeText(Register.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                } else {

                    sharedHelperModel.setAccessToken("Bearer " + response.optString("access_token"));
                    sharedHelperModel.setKeysCount(response.optString("keys_no"));
                    sharedHelperModel.setUserReferalCode(response.optString("referral_code"));
                    sharedHelperModel.setReferalCodeStatus(response.optString("referral_code_status"));
                    sharedHelperModel.setLoginStatus("1");
                    sharedHelperModel.setLoginStudentOrTeacher(response.optString("type"));
                    Intent intent = new Intent(Register.this, ChooseMedium.class);
                    startActivity(intent);
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("mobile_no", sharedHelperModel.getMobileNumber());
        body.put("otp", sharedHelperModel.getOtp());
        body.put("name", nameEdit.getText().toString().trim());
        body.put("gender", selectGender.getSelectedItem().toString().trim());
        body.put("type", type.toLowerCase());
        if (standard.equals("teacher")) {
            body.put("standard_teacher", "10");
            body.put("subject", selectedSubjects.toString());
        }
        body.put("school", schoolEdit.getText().toString().trim());
        body.put("district", districtEdit.getText().toString().trim());
        body.put("referral_code", referalCodeEdit.getText().toString().trim());
        body.put("device_id", sharedHelperModel.getDeviceToken());
        body.put("email", emailEdit.getText().toString().trim());
        body.put("standard", getResources().getString(R.string.standard));
        body.put("profile_image", message);

        HashMap<String, String> header = new HashMap<String, String>();
        header.put("authorization", "");

        Log.e(TAG, "volleyResponse: " + body);
        mVolleyService.postDataVolley("POSTCALL", UrlHelper.singup, body, header);

    }

    private void initViews() {
        registerButton = findViewById(R.id.registerButton);
        selectGender = findViewById(R.id.genderSpinner);
        nameEdit = findViewById(R.id.nameEdit);
        schoolEdit = findViewById(R.id.schoolEdit);
        referalCodeEdit = findViewById(R.id.referalCodeEdit);
        districtEdit = findViewById(R.id.districtEdit);
        sharedHelperModel = new SharedHelperModel(Register.this);
        imagePickButton = findViewById(R.id.imagePickButton);
        profilePicture = findViewById(R.id.profilePicture);
        whoAreYouRadioGroup = findViewById(R.id.whoAreYouRadioGroup);
        teacherLayout = findViewById(R.id.teacherLayout);
        selectStandardRadioGroup = findViewById(R.id.selectStandardRadioGroup);
        groupLayout = findViewById(R.id.groupLayout);
        groupSpinner = findViewById(R.id.groupSpinner);
        emailEdit = findViewById(R.id.emailEdit);
        setSubjectRecyclerview = findViewById(R.id.subjectsRecyclerView);


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void onStart() {
        super.onStart();
        try {
            EventBus.getDefault().register(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            EventBus.getDefault().unregister(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(SelectedSubjectArray event) {
        Log.e(TAG, "onMessageEvent:" + event.getSelectedArray());

        newSelectedArray = event.getSelectedArray();


    }

    public class Subjects {
        private String subjectName;

        public String getSubjectName() {
            return subjectName;
        }

        public String getSubjectId() {
            return subjectId;
        }

        private String subjectId;
        private boolean isSelected;

        public Subjects(String subjectName, String subjectId, boolean isSelected) {
            this.subjectName = subjectName;
            this.isSelected = isSelected;
            this.subjectId = subjectId;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }
    }
}
