package com.sura.tenthapp.modules.login_register.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sura.tenthapp.utils.checkPasswordMatches
import com.sura.tenthapp.utils.checkTextLengthMatchesRange

class LoginOrRegisterVM  : ViewModel(){
    var newPassword : String = ""
    var confirmPassword : String = ""
    var liveErrorMessage : MutableLiveData<String> = MutableLiveData()

    fun validatePassword() : Boolean{
        if(validateNewPassword() && validateConfirmPassword()){
            if(checkPasswordMatches(newPassword, confirmPassword)){
                return true
            }else {
                liveErrorMessage.value = "Password doesn't match"
            }
        }
        return false
    }

    fun validateNewPassword() : Boolean {
        if(!newPassword.isNullOrBlank() ){
            if( checkTextLengthMatchesRange(newPassword, 8,15)){
                return true
            }else{
                liveErrorMessage.value = "Please enter valid password"
            }
        }else{
            liveErrorMessage.value = "Please enter password"
        }
        return false
    }
    fun validateConfirmPassword() : Boolean {
        if(!confirmPassword.isNullOrBlank() ){
            if( checkTextLengthMatchesRange(confirmPassword, 8,15)){
                return true
            }else{
                liveErrorMessage.value = "Please enter valid confirm password"
            }
        }else{
            liveErrorMessage.value = "Please enter confirm password"
        }
        return false
    }
}