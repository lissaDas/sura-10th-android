package com.sura.tenthapp.modules.reading.chapter_contents;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.sura.tenthapp.R;
import com.sura.tenthapp.modules.ask_doubt.AskDoubt;
import com.sura.tenthapp.utils.language.BaseActivity;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.sura.tenthapp.utils.Utils.referral_concept_enabled;

public class BreadCrumbs extends BaseActivity {

    private static final String TAG = "BreadCrumsActivity";
    private RecyclerView breadCrumbsRecyclerView;
    private RecyclerView.Adapter BreadCrumbsAdapter;
    private Intent intent;
    public static String chapterId = "", chapterName = "", chapterNumber = "",subjectId="",subId="";
    private TextView chapterNumberText, chapterNameText;
    public static TextView keysText;
    private RelativeLayout backButton;
//    private AdView mAdView;
    private FirebaseAnalytics mFirebaseAnalytics;
    private RelativeLayout adLayout;
    public static JSONObject offlineJsonObject;
    private String isOffline,offlineArray;
    private Button askDoubtButton;
    private String subjectName="",subjectImageName="",subjectDivisionImageName="",subjectDivisionName="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bread_crumbs);

//        mAdView = findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

//        InMobiSdk.init(this, "482e77618f9b475b8d4701af71217434");
//        InMobiSdk.setLogLevel(InMobiSdk.LogLevel.DEBUG);
//
//        InMobiBanner bannerAd = new InMobiBanner(this, PlacementId.YOUR_PLACEMENT_ID);
//        RelativeLayout adContainer = findViewById(R.id.ad_container);
//        float density = getResources().getDisplayMetrics().density;
//        RelativeLayout.LayoutParams bannerLp = new RelativeLayout.LayoutParams((int) (320 * density), (int) (50 * density));
//        bannerLp.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//        adContainer.addView(bannerAd, bannerLp);
//        bannerAd.load();
//
//        bannerAd.setListener(new InMobiBanner.BannerAdListener() {
//            @Override
//            public void onAdLoadSucceeded(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdLoadSucceeded");
//            }
//
//            @Override
//            public void onAdLoadFailed(InMobiBanner inMobiBanner,
//                                       InMobiAdRequestStatus inMobiAdRequestStatus) {
//                Log.d(TAG, "Banner ad failed to load with error: " +
//                        inMobiAdRequestStatus.getMessage());
//            }
//
//            @Override
//            public void onAdDisplayed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDisplayed");
//            }
//
//            @Override
//            public void onAdDismissed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDismissed");
//            }
//
//            @Override
//            public void onAdInteraction(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdInteraction");
//            }
//
//            @Override
//            public void onUserLeftApplication(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onUserLeftApplication");
//            }
//
//            @Override
//            public void onAdRewardActionCompleted(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdRewardActionCompleted");
//            }
//        });



        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);

        LinearLayout keyDetails=findViewById(R.id.keyDetails);
        if(referral_concept_enabled){

            if(new SharedHelperModel(this).getIsOfferApplied().equalsIgnoreCase("true")){
                keyDetails.setVisibility(View.GONE);
            }else if(new SharedHelperModel(this).getIsOfferApplied().equalsIgnoreCase("false")){
                keyDetails.setVisibility(View.VISIBLE);
            }
        }else{
            keyDetails.setVisibility(View.GONE);
        }

        initViews();
        setListener();

        intent = getIntent();
        chapterId = intent.getStringExtra("chapterId");
        chapterName = intent.getStringExtra("chapterName");
        chapterNumber = intent.getStringExtra("chapterNumber");
        subId=intent.getStringExtra("subjectId");
        isOffline=intent.getStringExtra("isOffline");
        offlineArray=intent.getStringExtra("offlineArray");

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            chapterNameText.setText(Html.fromHtml(chapterName,Html.FROM_HTML_MODE_LEGACY));
        } else {
            chapterNameText.setText(Html.fromHtml(chapterName));
        }

//        chapterNumberText.setText(chapterNumber);

        Log.e(TAG, "onCreate: " + new SharedHelperModel(this).getAccessToken());

        keysText.setText(new SharedHelperModel(this).getKeysCount() + " Keys");

        if(isOffline.equalsIgnoreCase("true")){
            try {
                JSONObject jsonObject=new JSONObject(offlineArray);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    chapterNameText.setText(Html.fromHtml(jsonObject.optString("chapter_name"),Html.FROM_HTML_MODE_LEGACY));
                } else {
                    chapterNameText.setText(Html.fromHtml(jsonObject.optString("chapter_name")));
                }
//                chapterNumberText.setText(jsonObject.optString("chapter_number"));
                chapterName=jsonObject.optString("chapter_name");
                chapterNumber=jsonObject.optString("chapter_number");
                chapterId=jsonObject.optString("chapter_id");
                subjectId=jsonObject.optString("subject_id");

                setAdapter(jsonObject.optJSONArray("list_chapter_content"), jsonObject.optString("favourites"),chapterId,"true");

            } catch (JSONException e) {

            }

        }else{

            getVolleyResponse();
            subjectName=intent.getStringExtra("subjectName");
            subjectImageName=intent.getStringExtra("subjectImageName");
            subjectDivisionImageName=intent.getStringExtra("subjectDivisionImageName");
            subjectDivisionName=intent.getStringExtra("subjectDivisionName");
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void getVolleyResponse() {
        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
               loaderDialog.dismiss();
                if (response.optString("error").equals("false")) {
                    offlineJsonObject=response;
                    setAdapter(response.optJSONArray("list_chapter_content"), response.optString("favourites"),chapterId,"false");
                } else {
                    Toast.makeText(BreadCrumbs.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                    BreadCrumbs.super.onBackPressed();
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {
                Log.e(TAG, "notifySuccessString: "+response );
            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
                if(!error.equalsIgnoreCase("Parse Error.. Please Try Again..")){
                    retryDialog(error);
                }
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(this);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("chapter_id", chapterId);

        mVolleyService.postDataVolley("POSTCALL", UrlHelper.getChapterContent, body, headers);

    }

    public void retryDialog(final String error) {
        try {
            final Dialog retryDialog = new Dialog(this);
            retryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            retryDialog.setContentView(R.layout.no_internetconnection_dialog);
            retryDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            retryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            retryDialog.getWindow().setGravity(Gravity.CENTER);
            TextView retryDialogText=retryDialog.findViewById(R.id.retryDialogText);
            retryDialogText.setText(error);
            Button retryButton = retryDialog.findViewById(R.id.retryButton);
            retryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    retryDialog.dismiss();
                    getVolleyResponse();
                }
            });
            retryDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setListener() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BreadCrumbs.super.onBackPressed();
            }
        });
        askDoubtButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(BreadCrumbs.this,AskDoubt.class);
                intent.putExtra("subjectId",subId);
                intent.putExtra("chapterId",chapterId);
                startActivity(intent);
            }
        });
    }

    private void initViews() {
        keysText = findViewById(R.id.keysText);
        backButton = findViewById(R.id.backButton);
        chapterNumberText = findViewById(R.id.chapterNumberText);
        chapterNameText = findViewById(R.id.chapterNameText);
        askDoubtButton = findViewById(R.id.askDoubtButton);
    }

    public void setAdapter(JSONArray response, String favourites, String chapterId,String isOffline) {
        BreadCrumsFragment breadCrumsFragment = new BreadCrumsFragment();
        BreadCrumsFragment.jsonArray = response;
        BreadCrumsFragment.check = "title";
        BreadCrumsFragment.fav = favourites;
        BreadCrumsFragment.chapterName = chapterName;
        BreadCrumsFragment.chapterNumber = chapterNumber;
        BreadCrumsFragment.chapterId =chapterId;
        BreadCrumsFragment.isOffline =isOffline;
        BreadCrumsFragment.subjectName =subjectName;
        BreadCrumsFragment.subjectImageName =subjectImageName;
        BreadCrumsFragment.subjectDivisionImageName =subjectDivisionImageName;
        BreadCrumsFragment.subjectDivisionName =subjectDivisionName;
        Fragment fragmentnewview = new BreadCrumsFragment();
        FragmentManager frMan = getSupportFragmentManager();
        FragmentTransaction frTr = frMan.beginTransaction();
        frTr.add(R.id.content, fragmentnewview);
        frTr.commit();
    }

    public static void setFragmentAdapter(FragmentManager fragmentActivity, JSONArray response) {
        BreadCrumsFragment breadCrumsFragment = new BreadCrumsFragment();
        BreadCrumsFragment.jsonArray = response;
        BreadCrumsFragment.check = "content";
        Fragment fragmentnewview = new BreadCrumsFragment();
        FragmentTransaction frTr = fragmentActivity.beginTransaction();
        frTr.setCustomAnimations(R.anim.fadein, R.anim.fadeout);
        frTr.add(R.id.content, fragmentnewview);
        frTr.addToBackStack("1");
        frTr.commit();
    }

    @Override
    public void onBackPressed() {

        int count = getFragmentManager().getBackStackEntryCount();

        Log.e(TAG, "backStackCount: " + count);

        if (count == 0) {
            super.onBackPressed();
            //additional code
        } else {
            getFragmentManager().popBackStack();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));

    }
}
