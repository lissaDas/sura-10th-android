package com.sura.tenthapp.modules.reading.chapter_contents;

import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sura.tenthapp.R;
import com.sura.tenthapp.activity.ReadingActivity;

import org.json.JSONArray;
import org.json.JSONException;

public class BreadCrumbsAdapter extends RecyclerView.Adapter<BreadCrumbsAdapter.ViewHolder> {

    private static final String TAG = BreadCrumbsAdapter.class.getSimpleName();
    public Context context;
    public JSONArray jsonArray;
    public String check = "";
    public String fav = "", chapterName = "", chapterNumber = "",chapterId="";
    public String isOffline;
    public String subjectName="",subjectImageName="",subjectDivisionImageName="",subjectDivisionName="";
    private int bgCount=0;

    public BreadCrumbsAdapter(Context breadCrumbs, JSONArray jsonArray, String check, String fav, String chapterNumber, String chapterName, String chapterId, String isOffline, String subjectName, String subjectImageName, String subjectDivisionImageName, String subjectDivisionName) {
        this.check = check;
        this.context = breadCrumbs;
        this.jsonArray = jsonArray;
        this.fav = fav;
        this.chapterName = chapterName;
        this.chapterNumber = chapterNumber;
        this.chapterId=chapterId;
        this.isOffline=isOffline;
        this.subjectName=subjectName;
        this.subjectImageName=subjectImageName;
        this.subjectDivisionImageName=subjectDivisionImageName;
        this.subjectDivisionName=subjectDivisionName;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.breadcrumbs_recyclerview, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

//        try {
//            if (check.equals("title")) {
//
//                Log.e("content", "onBindViewHolder: " + jsonArray.optJSONObject(position).optString("title_content"));
//
//                if (jsonArray.optJSONObject(position).optString("title_content").equals("0")) {
//                    holder.subjectText.setText(jsonArray.optJSONObject(position).optString("name"));
//                    holder.titleSubjectText.setVisibility(View.GONE);
//                } else {
//
//                    if (jsonArray.optJSONObject(position).getJSONArray("title").length() > 1) {
//                        holder.subjectText.setText(jsonArray.optJSONObject(position).optString("name"));
//                        holder.titleSubjectText.setVisibility(View.GONE);
//                    } else {
//
//                        holder.titleSubjectText.setVisibility(View.VISIBLE);
//                        holder.subjectText.setText(jsonArray.optJSONObject(position).optString("name"));
//                        holder.titleSubjectText.setText(jsonArray.optJSONObject(position).optJSONArray("title").optJSONObject(0).optString("title"));
//                    }
//                }
//            } else {
//                holder.titleSubjectText.setVisibility(View.GONE);
//                if (jsonArray.optJSONObject(position).optString("title").equals("none")) {
//                    holder.subjectButton.setVisibility(View.GONE);
//                } else {
//                    holder.subjectButton.setVisibility(View.VISIBLE);
//                }
//                holder.subjectText.setText(jsonArray.optJSONObject(position).optString("title"));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.e("BreadCrumbsAdapter", "onBindViewHolder: " + e.getMessage());
//        }

        try {
            if (check.equals("title")) {
                Log.e("content", "onBindViewHolder: " + jsonArray.optJSONObject(position).optString("title_content"));

                if (jsonArray.optJSONObject(position).optString("title_content").equals("0")) {
//                    holder.subjectText.setText(jsonArray.optJSONObject(position).optString("name"));
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        holder.subjectText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optString("name"), Html.FROM_HTML_MODE_LEGACY));
                    } else {
                        holder.subjectText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optString("name")));
                    }
                    holder.titleSubjectText.setVisibility(View.GONE);
                } else {
                    if (jsonArray.optJSONObject(position).getJSONArray("title").length() > 1) {
//                        holder.subjectText.setText(jsonArray.optJSONObject(position).optString("name"));
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            holder.subjectText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optString("name"), Html.FROM_HTML_MODE_LEGACY));
                        } else {
                            holder.subjectText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optString("name")));
                        }
                        holder.titleSubjectText.setVisibility(View.GONE);
                    } else {
                        holder.titleSubjectText.setVisibility(View.VISIBLE);
//                        holder.subjectText.setText(jsonArray.optJSONObject(position).optString("name"));
//                        holder.titleSubjectText.setText(jsonArray.optJSONObject(position).optJSONArray("title").optJSONObject(0).optString("title"));
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            holder.subjectText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optString("name"), Html.FROM_HTML_MODE_LEGACY));
                            holder.titleSubjectText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optJSONArray("title").optJSONObject(0).optString("title"), Html.FROM_HTML_MODE_LEGACY));
                        } else {
                            holder.subjectText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optString("name")));
                            holder.titleSubjectText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optJSONArray("title").optJSONObject(0).optString("title")));
                        }
                    }
                }
            } else {
                holder.titleSubjectText.setVisibility(View.GONE);
                if (jsonArray.optJSONObject(position).optString("title").equals("none")) {
                    holder.subjectButton.setVisibility(View.GONE);
                } else {
                    holder.subjectButton.setVisibility(View.VISIBLE);
                }
//                holder.subjectText.setText(jsonArray.optJSONObject(position).optString("title"));
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    holder.subjectText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optString("title"), Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.subjectText.setText(Html.fromHtml(jsonArray.optJSONObject(position).optString("title")));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("BreadCrumbsAdapter", "onBindViewHolder: " + e.getMessage());
        }

        int[] rainbow = context.getResources().getIntArray(R.array.bgColors);
        holder.subjectButton.setBackgroundColor(rainbow[bgCount]);
        bgCount+=1;
        if(rainbow.length-1==bgCount){
            bgCount=0;
        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView subjectText, subSubjectText, titleSubjectText;
        public RelativeLayout subjectButton, subSubjectButton;

        public ViewHolder(View itemView) {
            super(itemView);
            subjectText = itemView.findViewById(R.id.subjectText);
            subjectButton = itemView.findViewById(R.id.subjectButton);
            titleSubjectText = itemView.findViewById(R.id.titleSubjectText);


//            subjectButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    FragmentManager manager = ((AppCompatActivity) context).getSupportFragmentManager();
//
//                    if (check.equals("title")) {
//
//                        if (jsonArray.optJSONObject(getAdapterPosition()).optString("title_content").equals("1")) {
////                            subSubjectButton.setVisibility(View.VISIBLE);
//                            try {
//                                if (jsonArray.optJSONObject(getAdapterPosition()).getJSONArray("title").length() > 1) {
//                                    BreadCrumbs.setFragmentAdapter(manager, jsonArray.optJSONObject(getAdapterPosition()).optJSONArray("title"));
//                                }else{
//
//                                    if (jsonArray.optJSONObject(getAdapterPosition()).optJSONArray("title").optJSONObject(0).optString("section").equals("0")) {
//                                        Intent intent = new Intent(context, ReadingActivity.class);
//                                        intent.putExtra("tableName", jsonArray.optJSONObject(getAdapterPosition()).optJSONArray("title").optJSONObject(0).optString("table_name"));
//                                        intent.putExtra("keyName", jsonArray.optJSONObject(getAdapterPosition()).optJSONArray("title").optJSONObject(0).optString("key_name"));
//                                        intent.putExtra("id", jsonArray.optJSONObject(getAdapterPosition()).optJSONArray("title").optJSONObject(0).optString("id"));
////                                        intent.putExtra("chapterId", jsonArray.optJSONObject(getAdapterPosition()).optJSONArray("title").optJSONObject(0).optString("chapter_id"));
//                                        intent.putExtra("chapterId", chapterId);
//                                        intent.putExtra("favCheck", fav);
//                                        intent.putExtra("chapterName", chapterName);
//                                        intent.putExtra("chapterNumber", chapterNumber);
//                                        intent.putExtra("isOffline",isOffline);
//                                        context.startActivity(intent);
//                                    } else {
//                                        BreadCrumbs.setFragmentAdapter(manager, jsonArray.optJSONObject(getAdapterPosition()).optJSONArray("title").optJSONObject(0).optJSONArray("section_content"));
//                                    }
//
//                                }
//                            } catch (JSONException e) {
//
//                            }
//
//
//                        } else {
//                            //Reading Activity intent goes here
//                            Log.e("Adapter", "TableName: " + jsonArray.optJSONObject(getAdapterPosition()).optString("tablename"));
//                            Intent intent = new Intent(context, ReadingActivity.class);
//                            intent.putExtra("tableName", jsonArray.optJSONObject(getAdapterPosition()).optString("tablename"));
//                            intent.putExtra("keyName", jsonArray.optJSONObject(getAdapterPosition()).optString("key_name"));
//                            intent.putExtra("id", jsonArray.optJSONObject(getAdapterPosition()).optString("id"));
////                            intent.putExtra("chapterId", jsonArray.optJSONObject(getAdapterPosition()).optString("chapter_id"));
//                            intent.putExtra("chapterId", chapterId);
//                            intent.putExtra("favCheck", fav);
//                            intent.putExtra("chapterName", chapterName);
//                            intent.putExtra("chapterNumber", chapterNumber);
//                            intent.putExtra("isOffline",isOffline);
//                            context.startActivity(intent);
//                        }
//                    } else {
//                        if (jsonArray.optJSONObject(getAdapterPosition()).optString("section").equals("1")) {
//                            BreadCrumbs.setFragmentAdapter(manager, jsonArray.optJSONObject(getAdapterPosition()).optJSONArray("section_content"));
//                        } else {
//                            //Reading Activity intent goes here
//                            Intent intent = new Intent(context, ReadingActivity.class);
//                            intent.putExtra("tableName", jsonArray.optJSONObject(getAdapterPosition()).optString("table_name"));
//                            intent.putExtra("keyName", jsonArray.optJSONObject(getAdapterPosition()).optString("key_name"));
//                            intent.putExtra("id", jsonArray.optJSONObject(getAdapterPosition()).optString("id"));
////                            intent.putExtra("chapterId", jsonArray.optJSONObject(getAdapterPosition()).optString("chapter_id"));
//                            intent.putExtra("chapterId", chapterId);
//                            intent.putExtra("favCheck", fav);
//                            intent.putExtra("chapterName", chapterName);
//                            intent.putExtra("chapterNumber", chapterNumber);
//                            intent.putExtra("isOffline",isOffline);
//                            context.startActivity(intent);
//                        }
//                    }
//                }
//            });

            subjectButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    FragmentManager manager = ((AppCompatActivity) context).getSupportFragmentManager();

                    if (check.equals("title")) {

                        if (jsonArray.optJSONObject(getAdapterPosition()).optString("title_content").equals("1")) {
//                            subSubjectButton.setVisibility(View.VISIBLE);
                            try {
                                if (jsonArray.optJSONObject(getAdapterPosition()).getJSONArray("title").length() > 1) {
                                    BreadCrumbs.setFragmentAdapter(manager, jsonArray.optJSONObject(getAdapterPosition()).optJSONArray("title"));
                                }else{

                                    if (jsonArray.optJSONObject(getAdapterPosition()).optJSONArray("title").optJSONObject(0).optString("section").equals("0")) {

                                        if(isOffline.equalsIgnoreCase("true")){
                                            Intent intent = new Intent(context, ReadingActivity.class);
                                            intent.putExtra("chapterContentJson",jsonArray.toString());
                                            intent.putExtra("tableName", jsonArray.optJSONObject(getAdapterPosition()).optJSONArray("title").optJSONObject(0).optString("table_name"));
                                            intent.putExtra("keyName", jsonArray.optJSONObject(getAdapterPosition()).optJSONArray("title").optJSONObject(0).optString("key_name"));
                                            intent.putExtra("id", jsonArray.optJSONObject(getAdapterPosition()).optJSONArray("title").optJSONObject(0).optString("id"));
                                            intent.putExtra("chapterId", BreadCrumbs.chapterId);
                                            intent.putExtra("chapterName", BreadCrumbs.chapterName);
                                            intent.putExtra("chapterNumber", BreadCrumbs.chapterNumber);
                                            intent.putExtra("isOffline",isOffline);

                                            intent.putExtra("subjectName",subjectName);
                                            intent.putExtra("subjectImageName",subjectImageName);
                                            intent.putExtra("subjectDivisionImageName",subjectDivisionImageName);
                                            intent.putExtra("subjectDivisionName",subjectDivisionName);
                                            context.startActivity(intent);
                                        }else{
                                            Intent intent = new Intent(context, ReadingActivity.class);
                                            intent.putExtra("chapterContentJson",jsonArray.toString());
                                            intent.putExtra("tableName", jsonArray.optJSONObject(getAdapterPosition()).optJSONArray("title").optJSONObject(0).optString("table_name"));
                                            intent.putExtra("keyName", jsonArray.optJSONObject(getAdapterPosition()).optJSONArray("title").optJSONObject(0).optString("key_name"));
                                            intent.putExtra("id", jsonArray.optJSONObject(getAdapterPosition()).optJSONArray("title").optJSONObject(0).optString("id"));
//                                        intent.putExtra("chapterId", jsonArray.optJSONObject(getAdapterPosition()).optJSONArray("title").optJSONObject(0).optString("chapter_id"));
                                            intent.putExtra("chapterId", chapterId);
                                            intent.putExtra("favCheck", fav);
                                            intent.putExtra("chapterName", chapterName);
                                            intent.putExtra("chapterNumber", chapterNumber);
                                            intent.putExtra("isOffline",isOffline);

                                            intent.putExtra("subjectName",subjectName);
                                            intent.putExtra("subjectImageName",subjectImageName);
                                            intent.putExtra("subjectDivisionImageName",subjectDivisionImageName);
                                            intent.putExtra("subjectDivisionName",subjectDivisionName);
                                            context.startActivity(intent);
                                        }


                                    } else {
                                        BreadCrumbs.setFragmentAdapter(manager, jsonArray.optJSONObject(getAdapterPosition()).optJSONArray("title").optJSONObject(0).optJSONArray("section_content"));
                                    }

                                }
                            } catch (JSONException e) {

                            }


                        } else {
                            //Reading Activity intent goes here
                            Log.e("Adapter", "TableName: " + jsonArray.optJSONObject(getAdapterPosition()).optString("tablename"));
                            if(isOffline.equalsIgnoreCase("true")){

                                Intent intent = new Intent(context, ReadingActivity.class);
                                intent.putExtra("chapterContentJson",jsonArray.toString());
                                intent.putExtra("tableName", jsonArray.optJSONObject(getAdapterPosition()).optString("tablename"));
                                intent.putExtra("keyName", jsonArray.optJSONObject(getAdapterPosition()).optString("key_name"));
                                intent.putExtra("id", jsonArray.optJSONObject(getAdapterPosition()).optString("id"));
                                intent.putExtra("chapterId", BreadCrumbs.chapterId);
                                intent.putExtra("chapterName", BreadCrumbs.chapterName);
                                intent.putExtra("chapterNumber", BreadCrumbs.chapterNumber);
                                intent.putExtra("isOffline",isOffline);

                                intent.putExtra("subjectName",subjectName);
                                intent.putExtra("subjectImageName",subjectImageName);
                                intent.putExtra("subjectDivisionImageName",subjectDivisionImageName);
                                intent.putExtra("subjectDivisionName",subjectDivisionName);
                                context.startActivity(intent);

                            }else{
                                Intent intent = new Intent(context, ReadingActivity.class);
                                intent.putExtra("chapterContentJson",jsonArray.toString());
                                intent.putExtra("tableName", jsonArray.optJSONObject(getAdapterPosition()).optString("tablename"));
                                intent.putExtra("keyName", jsonArray.optJSONObject(getAdapterPosition()).optString("key_name"));
                                intent.putExtra("id", jsonArray.optJSONObject(getAdapterPosition()).optString("id"));
//                            intent.putExtra("chapterId", jsonArray.optJSONObject(getAdapterPosition()).optString("chapter_id"));
                                intent.putExtra("chapterId", chapterId);
                                intent.putExtra("favCheck", fav);
                                intent.putExtra("chapterName", chapterName);
                                intent.putExtra("chapterNumber", chapterNumber);
                                intent.putExtra("isOffline",isOffline);

                                intent.putExtra("subjectName",subjectName);
                                intent.putExtra("subjectImageName",subjectImageName);
                                intent.putExtra("subjectDivisionImageName",subjectDivisionImageName);
                                intent.putExtra("subjectDivisionName",subjectDivisionName);
                                context.startActivity(intent);
                            }

                        }
                    } else {
                        if (jsonArray.optJSONObject(getAdapterPosition()).optString("section").equals("1")) {
                            BreadCrumbs.setFragmentAdapter(manager, jsonArray.optJSONObject(getAdapterPosition()).optJSONArray("section_content"));
                        } else {
                            //Reading Activity intent goes here
                            if(isOffline.equalsIgnoreCase("true")){

                                Intent intent = new Intent(context, ReadingActivity.class);
                                intent.putExtra("chapterContentJson",jsonArray.toString());
                                intent.putExtra("tableName", jsonArray.optJSONObject(getAdapterPosition()).optString("table_name"));
                                intent.putExtra("keyName", jsonArray.optJSONObject(getAdapterPosition()).optString("key_name"));
                                intent.putExtra("id", jsonArray.optJSONObject(getAdapterPosition()).optString("id"));
                                intent.putExtra("chapterId", BreadCrumbs.chapterId);
                                intent.putExtra("chapterName", BreadCrumbs.chapterName);
                                intent.putExtra("chapterNumber", BreadCrumbs.chapterNumber);
                                intent.putExtra("isOffline",isOffline);

                                intent.putExtra("subjectName",subjectName);
                                intent.putExtra("subjectImageName",subjectImageName);
                                intent.putExtra("subjectDivisionImageName",subjectDivisionImageName);
                                intent.putExtra("subjectDivisionName",subjectDivisionName);
                                context.startActivity(intent);

                            }else{
                                Intent intent = new Intent(context, ReadingActivity.class);
                                intent.putExtra("chapterContentJson",jsonArray.toString());
                                intent.putExtra("tableName", jsonArray.optJSONObject(getAdapterPosition()).optString("table_name"));
                                intent.putExtra("keyName", jsonArray.optJSONObject(getAdapterPosition()).optString("key_name"));
                                intent.putExtra("id", jsonArray.optJSONObject(getAdapterPosition()).optString("id"));
//                            intent.putExtra("chapterId", jsonArray.optJSONObject(getAdapterPosition()).optString("chapter_id"));
                                intent.putExtra("chapterId", chapterId);
                                intent.putExtra("favCheck", fav);
                                intent.putExtra("chapterName", chapterName);
                                intent.putExtra("chapterNumber", chapterNumber);
                                intent.putExtra("isOffline",isOffline);

                                intent.putExtra("subjectName",subjectName);
                                intent.putExtra("subjectImageName",subjectImageName);
                                intent.putExtra("subjectDivisionImageName",subjectDivisionImageName);
                                intent.putExtra("subjectDivisionName",subjectDivisionName);
                                context.startActivity(intent);
                            }

                        }
                    }
                }
            });


        }
    }
} 