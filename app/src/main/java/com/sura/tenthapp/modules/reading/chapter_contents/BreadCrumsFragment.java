package com.sura.tenthapp.modules.reading.chapter_contents;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sura.tenthapp.R;

import org.json.JSONArray;

public class BreadCrumsFragment extends Fragment {

    private static final String TAG = "BreadCrumbFragment";
    public RecyclerView breadCrumbsRecyclerView,breadCrumbsPathRecyclerView;
    public RecyclerView.Adapter BreadCrumbsAdapter,breadCrumbsPathAdapter;
    public static JSONArray jsonArray;
    public static String check="";
    public static String fav="";
    public static String chapterName="",chapterNumber="",chapterId="";
    public static String isOffline;
    public static String subjectName,subjectImageName,subjectDivisionImageName,subjectDivisionName;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.breadcrumbs_fragment, container, false);

        breadCrumbsRecyclerView=view.findViewById(R.id.breadCrumbsRecyclerView);
        breadCrumbsPathRecyclerView=view.findViewById(R.id.breadCrumbsPathRecyclerView);

            setBreadCombsAdapter();


        return view;
    }

    private void setBreadCombsAdapter() {

        if(jsonArray!=null){
            BreadCrumbsAdapter=new BreadCrumbsAdapter(getActivity(),jsonArray,check,fav,chapterNumber,chapterName,chapterId,isOffline,subjectName,subjectImageName,subjectDivisionImageName,subjectDivisionName);
            breadCrumbsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));
            breadCrumbsRecyclerView.setAdapter(BreadCrumbsAdapter);
        }

    }

//    private void setBreadCrumbsPathAdapter(){
//
//        breadCrumbsPathAdapter=new BreadCrumbsPathAdapter();
//        breadCrumbsPathRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
//        breadCrumbsPathRecyclerView.setAdapter(breadCrumbsPathAdapter);
//    }


} 