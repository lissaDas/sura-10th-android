package com.sura.tenthapp.modules.reading.chapter_list;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.sura.tenthapp.R;
import com.sura.tenthapp.modules.activity.SearchActivity;
import com.sura.tenthapp.modules.ask_doubt.AskDoubt;
import com.sura.tenthapp.utils.language.BaseActivity;
import com.sura.tenthapp.utils.DatabasePayment;
import com.sura.tenthapp.utils.KeyDetailsDialog;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.sura.tenthapp.utils.Utils.referral_concept_enabled;

public class ChapterList extends BaseActivity {

    private static RecyclerView chapterListRecyclerView;
    private static RecyclerView.Adapter chapterListAdapter;
    private static String TAG ="ChapterList";
    private static Intent intent;
    private static String subjectDivisionId="",subjectCost="";
    private static String subject_id="",subjectName="";
    private static Button askDoubtButton;
    private static RelativeLayout backButton;
    public static TextView keysText;
    private static TextView subjectNameText;
    private  LinearLayout searchButton;
//    private AdView mAdView;
    private FirebaseAnalytics mFirebaseAnalytics;
    private RelativeLayout adLayout;
    private String subjectPaymentId="";
    private DatabasePayment dbHelper;
    private ImageView downArrow;
    private String subjectDivisionImageName="",subjectDivisionName="",subjectImageName="";
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chapter_list);

//        mAdView = findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

//        InMobiSdk.init(this, "482e77618f9b475b8d4701af71217434");
//        InMobiSdk.setLogLevel(InMobiSdk.LogLevel.DEBUG);
//
//        InMobiBanner bannerAd = new InMobiBanner(this, PlacementId.YOUR_PLACEMENT_ID);
//        RelativeLayout adContainer = findViewById(R.id.ad_container);
//        float density = getResources().getDisplayMetrics().density;
//        RelativeLayout.LayoutParams bannerLp = new RelativeLayout.LayoutParams((int) (320 * density), (int) (50 * density));
//        bannerLp.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//        adContainer.addView(bannerAd, bannerLp);
//        bannerAd.load();
//
//        bannerAd.setListener(new InMobiBanner.BannerAdListener() {
//            @Override
//            public void onAdLoadSucceeded(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdLoadSucceeded");
//            }
//
//            @Override
//            public void onAdLoadFailed(InMobiBanner inMobiBanner,
//                                       InMobiAdRequestStatus inMobiAdRequestStatus) {
//                Log.d(TAG, "Banner ad failed to load with error: " +
//                        inMobiAdRequestStatus.getMessage());
//            }
//
//            @Override
//            public void onAdDisplayed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDisplayed");
//            }
//
//            @Override
//            public void onAdDismissed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDismissed");
//            }
//
//            @Override
//            public void onAdInteraction(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdInteraction");
//            }
//
//            @Override
//            public void onUserLeftApplication(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onUserLeftApplication");
//            }
//
//            @Override
//            public void onAdRewardActionCompleted(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdRewardActionCompleted");
//            }
//        });



        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);

        initViews();
        setListener();
        keyDialogListener();

        intent=getIntent();
        subjectDivisionId=intent.getStringExtra("subjectDivisionId");
        subject_id=intent.getStringExtra("subjectId");
        Log.e(TAG, "subjectId: "+subject_id );
        subjectName=intent.getStringExtra("subjectName");
        subjectCost=intent.getStringExtra("subjectCost");
        subjectPaymentId=intent.getStringExtra("subjectPaymentId");
        subjectNameText.setText(subjectName);

        keysText.setText(new SharedHelperModel(this).getKeysCount()+" Keys");

        LinearLayout keyDetails=findViewById(R.id.keyDetails);
        if(referral_concept_enabled){

            if(new SharedHelperModel(this).getIsOfferApplied().equalsIgnoreCase("true")){
                keyDetails.setVisibility(View.GONE);
            }else if(new SharedHelperModel(this).getIsOfferApplied().equalsIgnoreCase("false")){
                keyDetails.setVisibility(View.VISIBLE);
            }
        }else{
            keyDetails.setVisibility(View.GONE);
        }

        if(intent.getStringExtra("from").equalsIgnoreCase("home")){
            subjectImageName=intent.getStringExtra("subjectImageName");
            subjectDivisionName=subjectName;
            subjectDivisionImageName=intent.getStringExtra("subjectImageName");

        }else{
            subjectImageName=intent.getStringExtra("subjectImageName");
            subjectDivisionName=intent.getStringExtra("subjectDivisionName");
            subjectDivisionImageName=intent.getStringExtra("subjectDivisionImageName");
            Log.e(TAG, "testing: "+subjectImageName+" "+subjectDivisionName+" "+subjectDivisionImageName );
        }

        checkPaymentDb();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        keysText.setText(new SharedHelperModel(this).getKeysCount() + " Keys");
        Log.e(TAG, "getChapterListFromBreadCrumbs: "+new SharedHelperModel(this).getChapterListFromBreadCrumbs() );
        if(new SharedHelperModel(this).getChapterListFromBreadCrumbs().equalsIgnoreCase("true")){
            new SharedHelperModel(this).setChapterListFromBreadCrumbs("false");
            checkPaymentDb();
        }
    }

    private void checkPaymentDb() {
        int count = dbHelper.checkDuplicate("pending");

        Log.e(TAG, "checkPaymentDbCount: "+count );

        if (count == 0) {
            volleyResponse();
        } else {
            volleyResponse();
            final JSONArray jsonArray = dbHelper.selectFromDatabase();

            Log.e(TAG, "checkPaymentDbJsonArray: "+jsonArray );

            for (int i = 0; i < jsonArray.length(); i++) {

                final Dialog loaderDialog = LoaderDialog.showLoader(ChapterList.this);

                final int finalI = i;
                IResult mResultCallback = new IResult() {
                    @Override
                    public void notifySuccess(String requestType, JSONObject response) {
                        Log.e(TAG, "Acknowledgement " + requestType);
                        Log.e(TAG, "Acknowledgement " + response);
                        loaderDialog.dismiss();
                        if (response.optString("error").equalsIgnoreCase("false")) {
//                               Toast.makeText(ChapterList.this, reason, Toast.LENGTH_SHORT).show();
                            Boolean status = dbHelper.deleteRow(requestType);
                            Log.e(TAG, "paymentDbResposne: " + status);

                        } else {
                            Toast.makeText(ChapterList.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                        }
//                        if (jsonArray.length() - 1 == finalI) {
//                            volleyResponse();
//                        }
                    }

                    @Override
                    public void notifySuccessString(String requestType, String response) {

                    }

                    @Override
                    public void notifyError(String requestType, String error) {
                        Log.e(TAG, "Acknowledgement " + error);
                        Log.e(TAG, "Acknowledgement " + "That didn't work!");
                        loaderDialog.dismiss();
                        Toast.makeText(ChapterList.this, error, Toast.LENGTH_SHORT).show();
                    }
                };

                VolleyService mVolleyService = new VolleyService(mResultCallback, ChapterList.this);
                SharedHelperModel sharedHelperModel = new SharedHelperModel(ChapterList.this);

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("authorization", sharedHelperModel.getAccessToken());

                String s = jsonArray.optJSONObject(i).optString("body");
                String[] arr = s.split(", ");
                HashMap<String, String> body = new HashMap<String, String>();
                for (String str : arr) {
                    str = str.replace("{", "").replace("}", "");
                    String[] splited = str.split("=");

                    body.put(splited[0], splited[1].trim());

                }

                Log.e(TAG, "checkPaymentDb: " + body);


                mVolleyService.postDataVolley(jsonArray.optJSONObject(i).optString("id"), UrlHelper.postPaymentAck, body, headers);

            }

        }
    }


    public void retryDialog(final String error) {
        try{
            final Dialog retryDialog = new Dialog(this);
            retryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            retryDialog.setContentView(R.layout.no_internetconnection_dialog);
            retryDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            retryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            retryDialog.getWindow().setGravity(Gravity.CENTER);
            Button retryButton=retryDialog.findViewById(R.id.retryButton);
            TextView retryDialogText=retryDialog.findViewById(R.id.retryDialogText);
            retryDialogText.setText(error);
            retryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    retryDialog.dismiss();

                    volleyResponse();

                }
            });
            retryDialog.show();

        }catch (Exception e){
            e.printStackTrace();
        }

    }


    private void volleyResponse() {

        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                setChapterListAdapter(response);

//                getAmountVolley(response);
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType,String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
                if(!error.equalsIgnoreCase("Parse Error.. Please Try Again..")){
                    retryDialog(error);
                }
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback,this);
        SharedHelperModel sharedHelperModel=new SharedHelperModel(this);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization",sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("subject_division_id",subjectDivisionId);
        mVolleyService.postDataVolley("POSTCALL", UrlHelper.getChapterList,body,headers);
    }

    private void getAmountVolley(final JSONObject responseChapterList) {
        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                subjectCost=response.optString("cost");
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType,String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
                if(!error.equalsIgnoreCase("Parse Error.. Please Try Again..")){
                    retryDialog(error);
                }
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback,this);
        SharedHelperModel sharedHelperModel=new SharedHelperModel(this);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization",sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("subject_id",subject_id);
        Log.e(TAG, "getAmountVolley: "+body );
        mVolleyService.postDataVolley("POSTCALL", UrlHelper.postSubjectAmount,body,headers);

    }

    private void keyDialogListener() {
        LinearLayout keyDetails=findViewById(R.id.keyDetails);
        keyDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyDetailsDialog.showDialog(ChapterList.this);
            }
        });
    }

    private void setListener() {
        askDoubtButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ChapterList.this,AskDoubt.class);
                intent.putExtra("subjectId",subject_id);
                startActivity(intent);
            }
        });
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChapterList.super.onBackPressed();
            }
        });
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ChapterList.this,SearchActivity.class);
                intent.putExtra("subjectId",subject_id);
                startActivity(intent);
            }
        });
    }

    private void setChapterListAdapter(JSONObject response) {
        linearLayoutManager=new LinearLayoutManager(this,RecyclerView.VERTICAL,false);

        chapterListAdapter=new ChapterListAdapter(this,response,subjectDivisionId,subjectCost,subjectPaymentId,subject_id,subjectName,subjectDivisionName,subjectDivisionImageName,subjectImageName);
        chapterListRecyclerView.setLayoutManager(linearLayoutManager);
        chapterListRecyclerView.setAdapter(chapterListAdapter);

        chapterListRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int total = linearLayoutManager.getItemCount();
                int firstVisibleItemCount = linearLayoutManager.findFirstVisibleItemPosition();
                int lastVisibleItemCount = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                Log.e(TAG, "onScrolled: "+lastVisibleItemCount );
                if(lastVisibleItemCount==total-1){
                    downArrow.setVisibility(View.GONE);
                }else{
                    downArrow.setVisibility(View.VISIBLE);

                }
            }
        });

    }

    private void initViews() {
        chapterListRecyclerView=findViewById(R.id.chapterListRecyclerView);
        askDoubtButton=findViewById(R.id.askDoubtButton);
        backButton=findViewById(R.id.backButton);
        keysText=findViewById(R.id.keysText);
        subjectNameText=findViewById(R.id.subjectName);
        searchButton=findViewById(R.id.searchButton);
        downArrow=findViewById(R.id.downArrow);

        dbHelper=new DatabasePayment(ChapterList.this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
