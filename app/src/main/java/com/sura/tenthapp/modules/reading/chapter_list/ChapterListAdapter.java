package com.sura.tenthapp.modules.reading.chapter_list;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.sura.tenthapp.R;
import com.sura.tenthapp.modules.reading.chapter_contents.BreadCrumbs;
import com.sura.tenthapp.modules.settings.ReferYourFriend;
import com.sura.tenthapp.utils.Constants;
import com.sura.tenthapp.utils.DatabasePayment;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import static com.sura.tenthapp.utils.Utils.referral_concept_enabled;

public class ChapterListAdapter extends RecyclerView.Adapter<ChapterListAdapter.ViewHolder> implements PaytmPaymentTransactionCallback {
    private static final String TAG = "ChapterListAdapter";
    public Context context;
    private Dialog unlockChapterDialog, buySubjectDialog, showSelectChapter,offerImageDialog;
    public JSONObject jsonObject;
    public int i = 1;
    public String subjectDivisionId = "", subjectCost = "", subjectPaymentId = "", chapterId = "",subjectId="";
    private String orderId = "";
    private int randomNum = 0;
    private int randomNumCusId = 0;
    RecyclerView recyclerViewChaptersList;
    RecyclerView.Adapter chapterListPaymentAdapter;
    public static TextView totalAmount;
    private JSONArray chapterListArray = null;
    JSONObject responseAmount;
    private String type="";
    int bgCount=0;

    private DatabasePayment dbHelper;
    private String subjectName,subjectImageName,subjectDivisionImageName,subjectDivisionName;

    public ChapterListAdapter(Context chapterList, JSONObject response, String subjectDivisionId, String subjectCost, String subjectPaymentId, String subject_id, String subjectName, String subjectDivisionName, String subjectDivisionImageName, String subjectImageName) {
        this.context = chapterList;
        this.jsonObject = response;
        this.subjectDivisionId = subjectDivisionId;
        this.subjectCost = subjectCost;
        this.subjectPaymentId = subjectPaymentId;
        this.subjectId=subject_id;
        this.subjectName=subjectName;
        this.subjectImageName=subjectImageName;
        this.subjectDivisionImageName=subjectDivisionImageName;
        this.subjectDivisionName=subjectDivisionName;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_list_recyclerview, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;


    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.chapterTitleText.setText(Html.fromHtml(jsonObject.optJSONArray("list_chapter").optJSONObject(position).optString("chapter_name"), Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.chapterTitleText.setText(Html.fromHtml(jsonObject.optJSONArray("list_chapter").optJSONObject(position).optString("chapter_name")));
        }

        holder.chapterNumberText.setText(jsonObject.optJSONArray("list_chapter").optJSONObject(position).optString("chapter_number"));
//        holder.chapterTitleText.setText(jsonObject.optJSONArray("list_chapter").optJSONObject(position).optString("chapter_name"));
        holder.countStudyText.setText(jsonObject.optJSONArray("list_chapter").optJSONObject(position).optString("count") + " Study");

        if (jsonObject.optJSONArray("list_chapter").optJSONObject(position).optString("chapter_status").equals("lock")) {
            holder.lockIcon.setVisibility(View.VISIBLE);
        } else {
            holder.lockIcon.setVisibility(View.GONE);
        }

        int[] rainbow = context.getResources().getIntArray(R.array.bgColors);
        holder.readingMode.setBackgroundColor(rainbow[bgCount]);
        bgCount+=1;
        if(rainbow.length-1==bgCount){
            bgCount=0;
        }

        i = i + 1;
    }

    @Override
    public int getItemCount() {
        return jsonObject.optJSONArray("list_chapter").length();
    }

    @Override
    public void onTransactionResponse(Bundle bundle) {
        Log.e(TAG, "onTransactionResponse: " + bundle.toString());
        String responseCode = String.valueOf(bundle.get("RESPCODE"));
        Log.e(TAG, "onTransactionResponse: " + responseCode);

        InputStreamReader is = null;
        try {
            is = new InputStreamReader(context.getAssets().open("paytmResponseCodes.csv"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        BufferedReader reader = new BufferedReader(is);
        try {
            reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String line;
        try {
            while ((line = reader.readLine()) != null) {

                String[] cols = line.split(",");
                Log.e(TAG, "CSVfiles: " + "Coulmn 0= " + cols[0] + " , Column 1=" + cols[1]);

                if (cols[0].trim().equalsIgnoreCase(responseCode)) {
                    if (responseCode.equalsIgnoreCase("01")) {
                        volleyAcknowledgement("success", cols[1]);
                    } else {
                        volleyAcknowledgement("cancelled", cols[1]);
                    }
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

//        if(responseCode.equalsIgnoreCase("01")){
//            Toast.makeText(context, "Transaction Successfull..", Toast.LENGTH_SHORT).show();
//            volleyAcknowledgement("success", "success");
//        }else if(responseCode.equalsIgnoreCase("141")){
//            Toast.makeText(context, "Transaction Cancelled..", Toast.LENGTH_SHORT).show();
//            volleyAcknowledgement("cancelled", "cancelled");
//        }else if(responseCode.equalsIgnoreCase("14111")){
//            Toast.makeText(context, "Transaction Cancelled..", Toast.LENGTH_SHORT).show();
//            volleyAcknowledgement("cancelled", "cancelled");
//        }else if(responseCode.equalsIgnoreCase("227")){
//            Toast.makeText(context, "Transaction failed..", Toast.LENGTH_SHORT).show();
//            volleyAcknowledgement("failure", "cancelled");
//        }else if(responseCode.equalsIgnoreCase("810")){
//            Toast.makeText(context, "Transaction Cancelled..", Toast.LENGTH_SHORT).show();
//            volleyAcknowledgement("cancelled", "cancelled");
//        }else if(responseCode.equalsIgnoreCase("8102")){
//            Toast.makeText(context, "Transaction Cancelled..", Toast.LENGTH_SHORT).show();
//            volleyAcknowledgement("cancelled","cancelled");
//        }else if(responseCode.equalsIgnoreCase("8103")){
//            Toast.makeText(context, "Transaction Cancelled..", Toast.LENGTH_SHORT).show();
//            volleyAcknowledgement("cancelled", "cancelled");
//        }

    }


    @Override
    public void networkNotAvailable() {
        Log.e(TAG, "networkNotAvailable: " + "Network");
        Toast.makeText(context, "Network Not Available.. Please Try Again Later..", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clientAuthenticationFailed(String s) {
        Log.e(TAG, "clientAuthenticationFailed: " + s);
        Toast.makeText(context, "Client Authentication Failed.. Please Try Again Later..", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void someUIErrorOccurred(String s) {
        Log.e(TAG, "someUIErrorOccurred: " + s);
        Toast.makeText(context, "Client Authentication Failed.. Please Try Again Later..", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorLoadingWebPage(int i, String s, String s1) {
        Log.e(TAG, "onErrorLoadingWebPage: " + s + s1 + i);
        Toast.makeText(context, "Error Loading Paytm.. Please Try Again Later..", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressedCancelTransaction() {
        Log.e(TAG, "onBackPressedCancelTransaction: ");
        Toast.makeText(context, "Transaction Cancelled..", Toast.LENGTH_SHORT).show();


        volleyAcknowledgement("cancelled", "Cancelled Back Pressed");
    }

    @Override
    public void onTransactionCancel(String s, Bundle bundle) {
        Log.e(TAG, "onTransactionCancel: " + s);
        Toast.makeText(context, "Transaction Cancelled..", Toast.LENGTH_SHORT).show();


        volleyAcknowledgement("cancelled", "On Transaction Cancelled");

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout readingMode;
        public TextView chapterNumberText;
        public TextView chapterTitleText, countStudyText;
        public ImageView lockIcon;

        public ViewHolder(View itemView) {
            super(itemView);

            //clearCookies(context);
            dbHelper=new DatabasePayment(context);

            readingMode = itemView.findViewById(R.id.readingMode);
            chapterNumberText = itemView.findViewById(R.id.chapterNumberText);
            chapterTitleText = itemView.findViewById(R.id.chapterTitleText);
            countStudyText = itemView.findViewById(R.id.countStudyText);
            lockIcon = itemView.findViewById(R.id.lockIcon);

            unlockChapterDialog = new Dialog(context);
            unlockChapterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            unlockChapterDialog.setContentView(R.layout.unlock_this_chapter_dialog);

            buySubjectDialog = new Dialog(context);
            buySubjectDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            buySubjectDialog.setContentView(R.layout.unlock_chapter_payment_dialog);

            showSelectChapter = new Dialog(context);
            showSelectChapter.requestWindowFeature(Window.FEATURE_NO_TITLE);
            showSelectChapter.setContentView(R.layout.select_chapter_dialog);

            offerImageDialog = new Dialog(context);
            offerImageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            offerImageDialog.setContentView(R.layout.offer_image_listener_dialog);

            readingMode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (jsonObject.optJSONArray("list_chapter").optJSONObject(getAdapterPosition()).optString("chapter_status").equals("lock")) {
                        unlockChapterDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                        unlockChapterDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        unlockChapterDialog.getWindow().setGravity(Gravity.CENTER);
                        Button confirmButton = unlockChapterDialog.findViewById(R.id.confirmButton);
                        Button cancelButton = unlockChapterDialog.findViewById(R.id.cancelButton);
                        confirmButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                unlockChapterDialog.dismiss();
                                postVolleyResponse(jsonObject.optJSONArray("list_chapter").optJSONObject(getAdapterPosition()).optString("chapter_id"), getAdapterPosition());
                            }
                        });
                        cancelButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                unlockChapterDialog.dismiss();
                            }
                        });
                        unlockChapterDialog.show();
                    } else {
                        new SharedHelperModel(context).setChapterListFromBreadCrumbs("true");
                        Intent i = new Intent(context, BreadCrumbs.class);
                        i.putExtra("isOffline", "false");
                        i.putExtra("chapterId", jsonObject.optJSONArray("list_chapter").optJSONObject(getAdapterPosition()).optString("chapter_id"));
                        i.putExtra("chapterName", jsonObject.optJSONArray("list_chapter").optJSONObject(getAdapterPosition()).optString("chapter_name"));
                        i.putExtra("chapterNumber", jsonObject.optJSONArray("list_chapter").optJSONObject(getAdapterPosition()).optString("chapter_number"));
                        i.putExtra("subjectId", subjectId);
                        i.putExtra("subjectName",subjectName);
                        i.putExtra("subjectImageName",subjectImageName);
                        i.putExtra("subjectDivisionImageName",subjectDivisionImageName);
                        i.putExtra("subjectDivisionName",subjectDivisionName);
                        context.startActivity(i);
                    }
                }
            });
        }
    }


    private void postVolleyResponse(String chapterId, final int adapterPosition) {
        final Dialog loaderDialog = LoaderDialog.showLoader(context);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "postVolleyResponse " + requestType);
                Log.e(TAG, "postVolleyResponse" + response);
                loaderDialog.dismiss();
                if (response.optString("error").equals("false")) {

                    if (response.optString("error_message").toLowerCase().equals("no keys to buy chapter")) {
                        unlockChapterDialog.dismiss();
                        buySubjectDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                        buySubjectDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        buySubjectDialog.getWindow().setGravity(Gravity.CENTER);
                        Button buySubject = buySubjectDialog.findViewById(R.id.buySubject);
                        Button referFriendButton = buySubjectDialog.findViewById(R.id.referFriendButton);
//                        buySubject.setText(context.getResources().getString(R.string.buySubject) + " using paytm");
                        buySubject.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                buySubjectDialog.dismiss();
                                showOfferDialog();

                            }
                        });
                        if(referral_concept_enabled){
                                referFriendButton.setVisibility(View.VISIBLE);
                        }else{
                            referFriendButton.setVisibility(View.GONE);
                        }
                        referFriendButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(context, ReferYourFriend.class);
                                context.startActivity(intent);
                            }
                        });
                        buySubjectDialog.show();
                    } else {
                        Toast.makeText(context, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                        ChapterList.keysText.setText(response.optString("keys_no") + " keys");
                        new SharedHelperModel(context).setKeysCount(response.optString("keys_no"));
                        new SharedHelperModel(context).setChapterListFromBreadCrumbs("true");
                        Intent i = new Intent(context, BreadCrumbs.class);
                        i.putExtra("isOffline", "false");
                        i.putExtra("chapterId", jsonObject.optJSONArray("list_chapter").optJSONObject(adapterPosition).optString("chapter_id"));
                        i.putExtra("chapterName", jsonObject.optJSONArray("list_chapter").optJSONObject(adapterPosition).optString("chapter_name"));
                        i.putExtra("chapterNumber", jsonObject.optJSONArray("list_chapter").optJSONObject(adapterPosition).optString("chapter_number"));
                        i.putExtra("subjectId", subjectId);

                        i.putExtra("subjectName",subjectName);
                        i.putExtra("subjectImageName",subjectImageName);
                        i.putExtra("subjectDivisionImageName",subjectDivisionImageName);
                        i.putExtra("subjectDivisionName",subjectDivisionName);
                        context.startActivity(i);
                        unlockChapterDialog.dismiss();
                    }
                } else {
                    Toast.makeText(context, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                    unlockChapterDialog.dismiss();
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "postVolleyResponse " + error);
                Log.e(TAG, "postVolleyResponse" + "That didn't work!");
                loaderDialog.dismiss();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, context);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(context);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("chapter_id", chapterId);
        body.put("chapter_status", "unlock");

        mVolleyService.postDataVolley("POSTCALL", UrlHelper.postUnlockChapter, body, headers);
    }

    private void showOfferDialog() {
        offerImageDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        offerImageDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        offerImageDialog.getWindow().setGravity(Gravity.CENTER);
        offerImageDialog.setCanceledOnTouchOutside(false);
        offerImageDialog.setCancelable(false);
        final TextView subjectOfferText = offerImageDialog.findViewById(R.id.subjectOfferText);
        final TextView appOfferText = offerImageDialog.findViewById(R.id.appOfferText);
        final TextView unlockGroupText = offerImageDialog.findViewById(R.id.unlockGroupText);
        final TextView originalAmountText = offerImageDialog.findViewById(R.id.originalAmountText);
        final TextView originalAmountSubjectText = offerImageDialog.findViewById(R.id.originalAmountSubjectText);
        LinearLayout appUnlockButton = offerImageDialog.findViewById(R.id.appUnlockButton);
        LinearLayout subjectUnlockButton = offerImageDialog.findViewById(R.id.subjectUnlockButton);
        LinearLayout buyChapter = offerImageDialog.findViewById(R.id.buyChapter);
        final LinearLayout offerLayout = offerImageDialog.findViewById(R.id.offerLayout);
        final LinearLayout progressLayout = offerImageDialog.findViewById(R.id.progressLayout);
        final RelativeLayout closeButton = offerImageDialog.findViewById(R.id.closeButton);

        offerLayout.setVisibility(View.GONE);
        progressLayout.setVisibility(View.VISIBLE);
        offerImageDialog.show();

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                offerImageDialog.dismiss();
            }
        });

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "postVolleyResponse " + requestType);
                Log.e(TAG, "postVolleyResponse" + response);
//                loaderDialog.dismiss();
                if (response.optString("error").equals("false")) {

                    responseAmount =response;

                    offerLayout.setVisibility(View.VISIBLE);
                    progressLayout.setVisibility(View.GONE);
                    unlockGroupText.setText("Unlock Full App");
                    originalAmountText.setText("Rs. " + response.optString("subject_actual_amount") + "/- ");
                    originalAmountText.setPaintFlags(originalAmountText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    subjectOfferText.setText(response.optString("subject_percentage") + "% Offer Amount = Rs." + response.optString("subject_offer_amount") + "/-");
                    originalAmountSubjectText.setText("Rs. " + response.optString("app_actual_amount") + "/-" );
                    originalAmountSubjectText.setPaintFlags(originalAmountSubjectText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    appOfferText.setText( response.optString("app_percentage") + "% Offer Amount = Rs." + response.optString("app_offer_amount") + "/-");

                } else {
                    Toast.makeText(context, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                    unlockChapterDialog.dismiss();
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "postVolleyResponse " + error);
                Log.e(TAG, "postVolleyResponse" + "That didn't work!");
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, context);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(context);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("subject_id", subjectId);

        Log.e(TAG, "showOfferDialog: "+body );

        mVolleyService.postDataVolley("POSTCALL", UrlHelper.postSubjectAmount, body, headers);

        appUnlockButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                offerImageDialog.dismiss();
                type="app";
                generateCheckSum(Integer.parseInt(responseAmount.optString("app_offer_amount")),"app");
//                generateCheckSum(1,"app");
            }
        });

        subjectUnlockButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                offerImageDialog.dismiss();
                type="subject";
                generateCheckSum(Integer.parseInt(responseAmount.optString("subject_offer_amount")),"subject");
//                generateCheckSum(1,"subject");

            }
        });

        buyChapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                offerImageDialog.dismiss();
                showSelectChapterDialog();

            }
        });
    }


    private void showSelectChapterDialog() {

        showSelectChapter.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        showSelectChapter.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        showSelectChapter.getWindow().setGravity(Gravity.CENTER);
        showSelectChapter.setCanceledOnTouchOutside(false);
        showSelectChapter.setCancelable(false);
        Button buySubject = showSelectChapter.findViewById(R.id.buySubject);
        RelativeLayout closeButton = showSelectChapter.findViewById(R.id.closeButton);
        totalAmount = showSelectChapter.findViewById(R.id.totalAmount);
        LinearLayout selectAlllayout = showSelectChapter.findViewById(R.id.selectAllLayout);
        final ImageView selectAllTick = showSelectChapter.findViewById(R.id.selectAllTick);
        final TextView selectAllText = showSelectChapter.findViewById(R.id.selectAllText);

        recyclerViewChaptersList = showSelectChapter.findViewById(R.id.recyclerViewChaptersList);
        chapterListPaymentAdapter = new ChapterLIstPaymentAdapter(context, jsonObject.optJSONArray("list_chapter"));
        recyclerViewChaptersList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        recyclerViewChaptersList.setAdapter(chapterListPaymentAdapter);

        selectAlllayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectAllTick.getVisibility() == View.GONE) {
                    selectAllTick.setVisibility(View.VISIBLE);
                    selectAllText.setText("Unselect All");
                    int cost = 0;
                    for (int i = 0; i < jsonObject.optJSONArray("list_chapter").length(); i++) {
                        try {
                            jsonObject.optJSONArray("list_chapter").optJSONObject(i).put("isSelected", "true");
                            if(jsonObject.optJSONArray("list_chapter").optJSONObject(i).optString("chapter_status").equalsIgnoreCase("lock")){
                                cost = cost + Integer.parseInt(jsonObject.optJSONArray("list_chapter").optJSONObject(i).optString("cost"));
                            }
                        } catch (JSONException e) {

                        }
                    }
                    ChapterLIstPaymentAdapter.jsonArray = jsonObject.optJSONArray("list_chapter");
                    ChapterLIstPaymentAdapter.cost = 0;
                    ChapterLIstPaymentAdapter.cost = cost;
                    totalAmount.setText(String.valueOf(ChapterLIstPaymentAdapter.cost));
                    chapterListPaymentAdapter.notifyDataSetChanged();
                } else if (selectAllTick.getVisibility() == View.VISIBLE) {
                    selectAllTick.setVisibility(View.GONE);
                    selectAllText.setText("Select All");
                    for (int i = 0; i < jsonObject.optJSONArray("list_chapter").length(); i++) {
                        try {
                            jsonObject.optJSONArray("list_chapter").optJSONObject(i).put("isSelected", "false");
                        } catch (JSONException e) {

                        }
                    }

                    ChapterLIstPaymentAdapter.jsonArray = jsonObject.optJSONArray("list_chapter");
                    ChapterLIstPaymentAdapter.cost = 0;
                    totalAmount.setText(String.valueOf(ChapterLIstPaymentAdapter.cost));
                    chapterListPaymentAdapter.notifyDataSetChanged();
                }

            }
        });

        buySubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ChapterLIstPaymentAdapter.cost == 0) {
                    Toast.makeText(context, "Please Select Chapters", Toast.LENGTH_SHORT).show();
                } else {

                    showSelectChapter.dismiss();


                    chapterListArray = new JSONArray();

                    for (int i = 0; i < ChapterLIstPaymentAdapter.jsonArray.length(); i++) {
                        JSONObject jsonObject1 = new JSONObject();

                        if(jsonObject.optJSONArray("list_chapter").optJSONObject(i).optString("chapter_status").equalsIgnoreCase("lock")) {

                            if (ChapterLIstPaymentAdapter.jsonArray.optJSONObject(i).optString("isSelected").equalsIgnoreCase("true")) {
                                Log.e(TAG, "CheckList: " + ChapterLIstPaymentAdapter.jsonArray.optJSONObject(i).optString("chapter_id"));
                                try {
                                    jsonObject1.put("chapter_id", ChapterLIstPaymentAdapter.jsonArray.optJSONObject(i).optString("chapter_id"));
                                    chapterListArray.put(jsonObject1);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.e(TAG, "onClick: " + e.getMessage());
                                }

                            }
                        }

                    }

                    Log.e(TAG, "onClick: " + chapterListArray);
                    type="chapter";
                    generateCheckSum(ChapterLIstPaymentAdapter.cost,"chapter");


                    for (int i = 0; i < jsonObject.optJSONArray("list_chapter").length(); i++) {
                        try {
                            jsonObject.optJSONArray("list_chapter").optJSONObject(i).put("isSelected", "false");
                        } catch (JSONException e) {

                        }
                    }

                    ChapterLIstPaymentAdapter.jsonArray = jsonObject.optJSONArray("list_chapter");
                    ChapterLIstPaymentAdapter.cost = 0;
                    totalAmount.setText(String.valueOf(ChapterLIstPaymentAdapter.cost));
                    chapterListPaymentAdapter.notifyDataSetChanged();
                }

            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < jsonObject.optJSONArray("list_chapter").length(); i++) {
                    try {
                        jsonObject.optJSONArray("list_chapter").optJSONObject(i).put("isSelected", "false");
                    } catch (JSONException e) {

                    }
                }

                ChapterLIstPaymentAdapter.jsonArray = jsonObject.optJSONArray("list_chapter");
                ChapterLIstPaymentAdapter.cost = 0;
                totalAmount.setText(String.valueOf(ChapterLIstPaymentAdapter.cost));
                chapterListPaymentAdapter.notifyDataSetChanged();

                showSelectChapter.dismiss();

            }
        });

        showSelectChapter.show();


    }

    private void generateCheckSum(final int cost,final String type) {

        randomNum = (int) (Math.random() * 9000) + 1000;
        randomNumCusId = (int) (Math.random() * 900000) + 100000;
        orderId = "SURA" + subjectDivisionId + randomNum;

        final Dialog loaderDialog = LoaderDialog.showLoader(context);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "checkSum " + requestType);
                Log.e(TAG, "checkSum" + response);
//                Log.e(TAG, "GenerateChecksum: "+response.optString("chacksomehash") );
                loaderDialog.dismiss();
                if (response.optString("error").equals("false")) {
                    verifyCheckSum(response,cost);
                } else {
                    Toast.makeText(context, context.getResources().getString(R.string.tryAgain), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "checkSum " + error);
                Log.e(TAG, "checkSum" + "That didn't work!");
                loaderDialog.dismiss();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, context);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(context);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("MID", Constants.VALUE_MID);
        body.put("ORDER_ID", orderId);
        body.put("CUST_ID", new SharedHelperModel(context).getMobileNumber() + "SURA" + randomNumCusId);
        body.put("INDUSTRY_TYPE_ID", Constants.VALUE_INDUSTYPE);
        body.put("CHANNEL_ID", Constants.VALUE_CHANNELID);
        body.put("TXN_AMOUNT", String.valueOf(cost));
        body.put("WEBSITE", Constants.VALUE_WEBSITE);
        body.put("standard", "10");
        body.put("action", "generatechecksum");
        body.put("type",type);
        if(type.equalsIgnoreCase("chapter")){
            body.put("chapter_id", chapterListArray.toString());
        }
        if(type.equalsIgnoreCase("subject")){
            body.put("subject_id",subjectId);
        }
        body.put("CALLBACK_URL", Constants.VALUE_CALLBACKURL + orderId);
        Log.e(TAG, "generateCheckSum: " + body);
        mVolleyService.postDataVolley("POSTCALL", Constants.DATA_URL_CHECKSUMGEN, body, headers);
    }

    private void verifyCheckSum(final JSONObject responseGenerate, final int cost) {

        final Dialog loaderDialog = LoaderDialog.showLoader(context);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "VolleyVerifyChecksum " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                if (response.optString("error_message").toLowerCase().equals("true")) {
                    Log.e(TAG, "VerifyChecksum: " + response.optString("error_message"));
                    payTmPayment(responseGenerate,cost);
                } else {
                    Toast.makeText(context, context.getResources().getString(R.string.tryAgain), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, context);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(context);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("MID", Constants.VALUE_MID);
        body.put("ORDER_ID", orderId);
        body.put("CUST_ID", new SharedHelperModel(context).getMobileNumber() + "SURA" + randomNumCusId);
        body.put("INDUSTRY_TYPE_ID", Constants.VALUE_INDUSTYPE);
        body.put("CHANNEL_ID", Constants.VALUE_CHANNELID);
        body.put("TXN_AMOUNT", String.valueOf(cost));
        body.put("WEBSITE", Constants.VALUE_WEBSITE);
        body.put("CHECKSUMHASH", responseGenerate.optString("chacksomehash"));
        body.put("action", "verifychecksum");
        body.put("standard", "10");
        body.put("type",type);
        if(type.equalsIgnoreCase("chapter")){
            body.put("chapter_id", chapterListArray.toString());
        }
        if(type.equalsIgnoreCase("subject")){
            body.put("subject_id",subjectId);
        }
        body.put("CALLBACK_URL", Constants.VALUE_CALLBACKURL + orderId);
        Log.e(TAG, "verifyCheckSum: " + body);
        mVolleyService.postDataVolley("POSTCALL", Constants.DATA_URL_CHECKSUMGEN, body, headers);
    }


    private void payTmPayment(JSONObject responseGenerate, int cost) {

        PaytmPGService pgService = null;
        pgService = PaytmPGService.getProductionService();
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("MID", Constants.VALUE_MID);
        paramMap.put("ORDER_ID", orderId);
        paramMap.put("CUST_ID", new SharedHelperModel(context).getMobileNumber() + "SURA" + randomNumCusId);
        paramMap.put("INDUSTRY_TYPE_ID", Constants.VALUE_INDUSTYPE);
        paramMap.put("CHANNEL_ID", Constants.VALUE_CHANNELID);
        paramMap.put("TXN_AMOUNT", String.valueOf(cost));
        paramMap.put("WEBSITE", Constants.VALUE_WEBSITE);
        paramMap.put("CALLBACK_URL", Constants.VALUE_CALLBACKURL + orderId);
        paramMap.put("CHECKSUMHASH", responseGenerate.optString("chacksomehash"));

        Log.e(TAG, "payTmPayment: " + paramMap);

        PaytmOrder Order = new PaytmOrder(paramMap);

        pgService.initialize(Order, null);

        pgService.startPaymentTransaction(context, true, true, this);

    }

    private void volleyAcknowledgement(final String paymentStatus, final String reason) {

        final Dialog loaderDialog = LoaderDialog.showLoader(context);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Acknowledgement " + requestType);
                Log.e(TAG, "Acknowledgement " + response);
                loaderDialog.dismiss();
                if (response.optString("error").equalsIgnoreCase("false")) {
                    Toast.makeText(context, reason, Toast.LENGTH_SHORT).show();
                    Boolean status=dbHelper.deleteRow(requestType);
                    Log.e(TAG, "paymentDbResposne: "+status );
                    volleyResponse();
                } else {
                    Toast.makeText(context, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Acknowledgement " + error);
                Log.e(TAG, "Acknowledgement " + "That didn't work!");
                loaderDialog.dismiss();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, context);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(context);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("action", "acknowledgement");
        body.put("payment_status", paymentStatus);
        body.put("trans_id", orderId);
        body.put("reason", reason);
        body.put("type",type);
        if(type.equalsIgnoreCase("chapter")){
            body.put("chapter_id", chapterListArray.toString());
        }
        if(type.equalsIgnoreCase("subject")){
            body.put("subject_id",subjectId);
        }
        long id=dbHelper.addToDatabase("pending",body.toString());

        mVolleyService.postDataVolley(String.valueOf(id), UrlHelper.postPaymentAck, body, headers);
    }


    private void volleyResponse() {

        final Dialog loaderDialog = LoaderDialog.showLoader(context);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                loaderDialog.dismiss();
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                i = 1;
                jsonObject = response;
                notifyDataSetChanged();

            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, context);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(context);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("subject_division_id", subjectDivisionId);

        mVolleyService.postDataVolley("POSTCALL", UrlHelper.getChapterList, body, headers);

    }
}