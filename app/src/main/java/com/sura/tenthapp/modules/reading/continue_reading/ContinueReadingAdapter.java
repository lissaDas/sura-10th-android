package com.sura.tenthapp.modules.reading.continue_reading;

import android.content.Context;
import android.content.Intent;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sura.tenthapp.R;
import com.sura.tenthapp.modules.reading.chapter_contents.BreadCrumbs;

import org.json.JSONArray;

public class ContinueReadingAdapter extends RecyclerView.Adapter<ContinueReadingAdapter.ViewHolder> {

    public Context context;
    public JSONArray jsonArray;

    public ContinueReadingAdapter(Context activity, JSONArray jsonArray) {
        this.context=activity;
        this.jsonArray=jsonArray;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.continue_study_recyclerview,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

                if(jsonArray.optJSONObject(position).optString("subject_division_image_name").toLowerCase().equals("tamil")){
            holder.backgroundContinue.setCardBackgroundColor(context.getResources().getColor(R.color.tamilColor));
            holder.subjectIcon.setBackground(context.getResources().getDrawable(R.drawable.tamil));
        }else if(jsonArray.optJSONObject(position).optString("subject_division_image_name").toLowerCase().equals("english")){
//            holder.backgroundContinue.setBackground(context.getResources().getDrawable(R.drawable.english_continue));
            holder.backgroundContinue.setCardBackgroundColor(context.getResources().getColor(R.color.englishColor));
            holder.subjectIcon.setBackground(context.getResources().getDrawable(R.drawable.english));

        }else if(jsonArray.optJSONObject(position).optString("subject_division_image_name").toLowerCase().equals("maths")){
//            holder.backgroundContinue.setBackground(context.getResources().getDrawable(R.drawable.maths_continue));
            holder.backgroundContinue.setCardBackgroundColor(context.getResources().getColor(R.color.mathsColor));
            holder.subjectIcon.setBackground(context.getResources().getDrawable(R.drawable.maths));

        }else if(jsonArray.optJSONObject(position).optString("subject_division_image_name").toLowerCase().equals("biology")){
//            holder.backgroundContinue.setBackground(context.getResources().getDrawable(R.drawable.biology_continue));
            holder.backgroundContinue.setCardBackgroundColor(context.getResources().getColor(R.color.biologyColor));
            holder.subjectIcon.setBackground(context.getResources().getDrawable(R.drawable.bio));

        }else if(jsonArray.optJSONObject(position).optString("subject_division_image_name").toLowerCase().equals("chemistry")){
//            holder.backgroundContinue.setBackground(context.getResources().getDrawable(R.drawable.chemistry_continue));
            holder.backgroundContinue.setCardBackgroundColor(context.getResources().getColor(R.color.chemistryColor));
            holder.subjectIcon.setBackground(context.getResources().getDrawable(R.drawable.chemistry));

        }else if(jsonArray.optJSONObject(position).optString("subject_division_image_name").toLowerCase().equals("physics")){
//            holder.backgroundContinue.setBackground(context.getResources().getDrawable(R.drawable.physics_continue));
            holder.backgroundContinue.setCardBackgroundColor(context.getResources().getColor(R.color.physicsColor));
            holder.subjectIcon.setBackground(context.getResources().getDrawable(R.drawable.physics));

        }else if(jsonArray.optJSONObject(position).optString("subject_division_image_name").toLowerCase().equals("history")){
//            holder.backgroundContinue.setBackground(context.getResources().getDrawable(R.drawable.history_continue));
            holder.backgroundContinue.setCardBackgroundColor(context.getResources().getColor(R.color.historyColor));
            holder.subjectIcon.setBackground(context.getResources().getDrawable(R.drawable.history));

        }else if(jsonArray.optJSONObject(position).optString("subject_division_image_name").toLowerCase().equals("geography")){
//            holder.backgroundContinue.setBackground(context.getResources().getDrawable(R.drawable.geography_continue));
            holder.backgroundContinue.setCardBackgroundColor(context.getResources().getColor(R.color.geographyColor));
            holder.subjectIcon.setBackground(context.getResources().getDrawable(R.drawable.geo));

        }else if(jsonArray.optJSONObject(position).optString("subject_division_image_name").toLowerCase().equals("civics")){
//            holder.backgroundContinue.setBackground(context.getResources().getDrawable(R.drawable.civics_continue));
            holder.backgroundContinue.setCardBackgroundColor(context.getResources().getColor(R.color.civisColor));
            holder.subjectIcon.setBackground(context.getResources().getDrawable(R.drawable.civics));

        }else if(jsonArray.optJSONObject(position).optString("subject_division_image_name").toLowerCase().equals("economics")){
//            holder.backgroundContinue.setBackground(context.getResources().getDrawable(R.drawable.economics_continue));
            holder.backgroundContinue.setCardBackgroundColor(context.getResources().getColor(R.color.economicsColor));
            holder.subjectIcon.setBackground(context.getResources().getDrawable(R.drawable.economics));

        }else{
            holder.backgroundContinue.setCardBackgroundColor(context.getResources().getColor(R.color.buttonColorBlue));

        }

        holder.subjectName.setText(jsonArray.optJSONObject(position).optString("subject_division_name"));
        holder.chapterNameText.setText(jsonArray.optJSONObject(position).optString("chapter_name"));
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public CardView backgroundContinue;
        public TextView subjectName,chapterNameText;
        private ImageView subjectIcon;
        public ViewHolder(View itemView) {
            super(itemView);

            backgroundContinue=itemView.findViewById(R.id.backgroundContinue);
            subjectName=itemView.findViewById(R.id.subjectName);
            chapterNameText=itemView.findViewById(R.id.chapterNameText);
            subjectIcon=itemView.findViewById(R.id.subjectIcon);

            backgroundContinue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i=new Intent(context, BreadCrumbs.class);
                    i.putExtra("isOffline","false");

                    i.putExtra("chapterId",jsonArray.optJSONObject(getAdapterPosition()).optString("chapter_id"));
                    i.putExtra("chapterName",jsonArray.optJSONObject(getAdapterPosition()).optString("chapter_name"));
                    i.putExtra("chapterNumber",jsonArray.optJSONObject(getAdapterPosition()).optString("chapter_number"));
                    i.putExtra("subjectDivisionName", jsonArray.optJSONObject(getAdapterPosition()).optString("subject_division_name"));
                    i.putExtra("subjectDivisionImageName", jsonArray.optJSONObject(getAdapterPosition()).optString("subject_division_image_name"));

                    context.startActivity(i);
                }
            });
        }
    }
}