package com.sura.tenthapp.modules.reading.subject_list_home;

import android.content.Context;
import android.content.Intent;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sura.tenthapp.R;
import com.sura.tenthapp.modules.reading.chapter_list.ChapterList;
import com.sura.tenthapp.modules.reading.sub_category_subject_list.SubCategorySubject;

import org.json.JSONException;
import org.json.JSONObject;

public class SubjectsHomeAdapter extends RecyclerView.Adapter<SubjectsHomeAdapter.ViewHolder> {

    private static final String TAG =SubjectsHomeAdapter.class.getSimpleName() ;
    public Context context;
    public JSONObject jsonObject;
    public static String subjectName="",subjectId="";

    public SubjectsHomeAdapter(Context homeFragment, JSONObject jsonObject, RecyclerView subjectsRecyclerView) {

        this.context = homeFragment;
        this.jsonObject = jsonObject;

//        subjectsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
//        {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
//            {
//                if (dy > 0 ||dy<0 && HomeActivity.bottomLayout.isShown())
//                {
//                    HomeActivity.bottomLayout.setVisibility(View.GONE);
//                }
//            }
//
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState)
//            {
//                if (newState == RecyclerView.SCROLL_STATE_IDLE)
//                {
//                    HomeActivity.bottomLayout.setVisibility(View.VISIBLE);
//                }
//
//                super.onScrollStateChanged(recyclerView, newState);
//            }
//        });
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.subjects_recyclerview, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

//        Log.e("Adapter", "onBindViewHolder: "+jsonObject.optJSONArray("list_subject").optJSONObject(position).optString("subject_name"));

        //Log.e("Adapter", "onBindViewHolder: "+jsonObject.optJSONArray("list_subject").optJSONObject(position).optString("subject_name"));
        try {
            holder.subjectText.setText(jsonObject.optJSONArray("list_subject").optJSONObject(position).optString("subject_name"));
            if (jsonObject.optJSONArray("list_subject").optJSONObject(position).optString("subject_image_name").toLowerCase().equals("tamil")) {
                holder.subjectButton.setCardBackgroundColor(context.getResources().getColor(R.color.tamilColor));
                holder.subjectIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.tamil));
            } else if (jsonObject.optJSONArray("list_subject").optJSONObject(position).optString("subject_image_name").toLowerCase().equals("english")) {
                holder.subjectButton.setCardBackgroundColor(context.getResources().getColor(R.color.englishColor));
                holder.subjectIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.english));
            } else if (jsonObject.optJSONArray("list_subject").optJSONObject(position).optString("subject_image_name").toLowerCase().equals("maths")) {
                holder.subjectButton.setCardBackgroundColor(context.getResources().getColor(R.color.mathsColor));
                holder.subjectIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.maths));
            } else if (jsonObject.optJSONArray("list_subject").optJSONObject(position).optString("subject_image_name").toLowerCase().equals("science")) {
                holder.subjectButton.setCardBackgroundColor(context.getResources().getColor(R.color.scienceColor));
                holder.subjectIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.science));
            } else if (jsonObject.optJSONArray("list_subject").optJSONObject(position).optString("subject_image_name").toLowerCase().equals("social science")) {
                holder.subjectButton.setCardBackgroundColor(context.getResources().getColor(R.color.socialColor));
                holder.subjectIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.social));
            }else{
                holder.subjectButton.setCardBackgroundColor(context.getResources().getColor(R.color.buttonColorBlue));
            }
        } catch (Exception e) {
            e.printStackTrace();

            Log.e("adapter", "onBindViewHolder: "+e.getMessage() );

        }

    }

    @Override
    public int getItemCount() {
        return jsonObject.optJSONArray("list_subject").length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout adLayout;
        public TextView subjectText;
        private ImageView subjectIcon;
        private CardView subjectButton;

        public ViewHolder(View itemView) {
            super(itemView);
            subjectButton = itemView.findViewById(R.id.subjectButton);
            subjectText = itemView.findViewById(R.id.subjectText);
            subjectIcon = itemView.findViewById(R.id.subjectIcon);




            subjectButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if (jsonObject.optJSONArray("list_subject").optJSONObject(getAdapterPosition()).optString("subject_division_state").equals("1")) {
                        Intent intent = new Intent(context, SubCategorySubject.class);
                        try {
                            subjectName=jsonObject.optJSONArray("list_subject").optJSONObject(getAdapterPosition()).optString("subject_image_name").toLowerCase();
                            subjectId=jsonObject.optJSONArray("list_subject").optJSONObject(getAdapterPosition()).optString("subject_id");

                            intent.putExtra("subcategory", String.valueOf(jsonObject.optJSONArray("list_subject").optJSONObject(getAdapterPosition()).getJSONArray("subject_division")));
                            intent.putExtra("subjectId", jsonObject.optJSONArray("list_subject").optJSONObject(getAdapterPosition()).optString("subject_id"));
                            intent.putExtra("subjectName", jsonObject.optJSONArray("list_subject").optJSONObject(getAdapterPosition()).optString("subject_name"));
                            intent.putExtra("subjectCost", jsonObject.optJSONArray("list_subject").optJSONObject(getAdapterPosition()).optString("subject_cost"));
                            intent.putExtra("subjectPaymentId", jsonObject.optJSONArray("list_subject").optJSONObject(getAdapterPosition()).optString("subject_payment_id"));
                            intent.putExtra("subjectImageName",jsonObject.optJSONArray("list_subject").optJSONObject(getAdapterPosition()).optString("subject_image_name"));

                            context.startActivity(intent);
                        } catch (JSONException e) {

                        }
                    } else {
                        subjectName=jsonObject.optJSONArray("list_subject").optJSONObject(getAdapterPosition()).optString("subject_image_name").toLowerCase();
                        subjectId=jsonObject.optJSONArray("list_subject").optJSONObject(getAdapterPosition()).optString("subject_id");

                        Intent intent = new Intent(context, ChapterList.class);
                        intent.putExtra("subjectDivisionId", jsonObject.optJSONArray("list_subject").optJSONObject(getAdapterPosition()).optString("subject_division_id"));
                        intent.putExtra("subjectId", jsonObject.optJSONArray("list_subject").optJSONObject(getAdapterPosition()).optString("subject_id"));
                        intent.putExtra("subjectName", jsonObject.optJSONArray("list_subject").optJSONObject(getAdapterPosition()).optString("subject_name"));
                        intent.putExtra("subjectCost", jsonObject.optJSONArray("list_subject").optJSONObject(getAdapterPosition()).optString("subject_cost"));
                        intent.putExtra("subjectPaymentId", jsonObject.optJSONArray("list_subject").optJSONObject(getAdapterPosition()).optString("subject_payment_id"));
                        intent.putExtra("subjectImageName",jsonObject.optJSONArray("list_subject").optJSONObject(getAdapterPosition()).optString("subject_image_name"));
                        intent.putExtra("from","home");

                        context.startActivity(intent);
                    }
                }
            });
        }
    }
}