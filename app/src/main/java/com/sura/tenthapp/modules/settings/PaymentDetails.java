package com.sura.tenthapp.modules.settings;

import android.app.Dialog;
import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sura.tenthapp.R;
import com.sura.tenthapp.utils.LoaderDialog;
import com.sura.tenthapp.utils.SharedHelperModel;
import com.sura.tenthapp.utils.UrlHelper;
import com.sura.tenthapp.utils.volley.IResult;
import com.sura.tenthapp.utils.volley.VolleyService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PaymentDetails extends AppCompatActivity {

    private static final String TAG = PaymentDetails.class.getSimpleName();
    private RecyclerView paymentDetailsRecyclerview;
    private RecyclerView.Adapter paymentDetailsAdapter;
    private TextView noPaymentList;
    private RelativeLayout backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_details);

//        AdView mAdView = findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

//        InMobiSdk.init(this, "482e77618f9b475b8d4701af71217434");
//        InMobiSdk.setLogLevel(InMobiSdk.LogLevel.DEBUG);
//
//        InMobiBanner bannerAd = new InMobiBanner(this, PlacementId.YOUR_PLACEMENT_ID);
//        RelativeLayout adContainer = findViewById(R.id.ad_container);
//        float density = getResources().getDisplayMetrics().density;
//        RelativeLayout.LayoutParams bannerLp = new RelativeLayout.LayoutParams((int) (320 * density), (int) (50 * density));
//        bannerLp.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//        adContainer.addView(bannerAd, bannerLp);
//        bannerAd.load();
//
//        bannerAd.setListener(new InMobiBanner.BannerAdListener() {
//            @Override
//            public void onAdLoadSucceeded(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdLoadSucceeded");
//            }
//
//            @Override
//            public void onAdLoadFailed(InMobiBanner inMobiBanner,
//                                       InMobiAdRequestStatus inMobiAdRequestStatus) {
//                Log.d(TAG, "Banner ad failed to load with error: " +
//                        inMobiAdRequestStatus.getMessage());
//            }
//
//            @Override
//            public void onAdDisplayed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDisplayed");
//            }
//
//            @Override
//            public void onAdDismissed(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onAdDismissed");
//            }
//
//            @Override
//            public void onAdInteraction(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdInteraction");
//            }
//
//            @Override
//            public void onUserLeftApplication(InMobiBanner inMobiBanner) {
//                Log.d(TAG, "onUserLeftApplication");
//            }
//
//            @Override
//            public void onAdRewardActionCompleted(InMobiBanner inMobiBanner, Map<Object, Object> map) {
//                Log.d(TAG, "onAdRewardActionCompleted");
//            }
//        });

        initViews();
        setListeners();
        getDoubtsVolleyResponse();

    }

    private void setListeners() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PaymentDetails.super.onBackPressed();
            }
        });
    }

    private void initViews() {
        paymentDetailsRecyclerview=findViewById(R.id.recyclerviewPayments);
        noPaymentList=findViewById(R.id.noPaymentList);
        backButton=findViewById(R.id.backButton);
    }

    private void getDoubtsVolleyResponse() {

        final Dialog loaderDialog = LoaderDialog.showLoader(this);

        IResult mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, JSONObject response) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + response);
                loaderDialog.dismiss();
                if (response.optString("error").equals("true")) {
                    Toast.makeText(PaymentDetails.this, response.optString("error_message"), Toast.LENGTH_SHORT).show();
                    if(response.optString("error_message").equalsIgnoreCase("No Invoice List.")){
                        noPaymentList.setVisibility(View.VISIBLE);
                    }
                } else {
                    noPaymentList.setVisibility(View.GONE);
                    setPaymentDetailsAdapter(response.optJSONArray("details"));

                }
            }

            @Override
            public void notifySuccessString(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, String error) {
                Log.e(TAG, "Volley requester " + error);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                loaderDialog.dismiss();
            }
        };

        VolleyService mVolleyService = new VolleyService(mResultCallback, this);
        SharedHelperModel sharedHelperModel = new SharedHelperModel(this);

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", sharedHelperModel.getAccessToken());

        HashMap<String, String> body = new HashMap<String, String>();
        body.put("id","");
        mVolleyService.postDataVolley("POSTCALL", UrlHelper.postUserInvoice, body, headers);

    }

    private void setPaymentDetailsAdapter(JSONArray jsonArray) {
        paymentDetailsAdapter = new PaymentDetailsAdapter(this, jsonArray);
        paymentDetailsRecyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        paymentDetailsRecyclerview.setAdapter(paymentDetailsAdapter);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


}
