package com.sura.tenthapp.modules.settings;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sura.tenthapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SubjectListProfileAdapter extends RecyclerView.Adapter<SubjectListProfileAdapter.ViewHolder> {

    private static final String TAG = SubjectListProfileAdapter.class.getSimpleName();
    public Context context;
    public static JSONArray jsonArray;
    public static JSONArray selectedJsonArray;


    public SubjectListProfileAdapter(Context context, JSONArray jsonArray) {
        this.context=context;
        SubjectListProfileAdapter.jsonArray =jsonArray;
        selectedJsonArray =new JSONArray();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.set_subject_register_recyclerview,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.subjectName.setText(jsonArray.optJSONObject(position).optString("subject_name"));

//        if(jsonArray.optJSONObject(position).optString("select_subject").equals("1")){
//            holder.checkbox.setChecked(true);
//            try {
//                jsonArray.optJSONObject(position).put("isSelected", "true");
//            } catch (JSONException e) {
//
//            }
//        }else{
//            holder.checkbox.setChecked(false);
//            try {
//                jsonArray.optJSONObject(position).put("isSelected", "false");
//            } catch (JSONException e) {
//
//            }
//        }

        if(jsonArray.optJSONObject(position).optString("isSelected").equals("true")){
            holder.checkbox.setChecked(true);
        }else{
            holder.checkbox.setChecked(false);
        }



        holder.clickSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    if (holder.checkbox.isChecked()) {
                        jsonArray.optJSONObject(position).put("isSelected", "false");
                        holder.checkbox.setChecked(false);
                        Log.e(TAG, "onClick: "+"deleted" );
                        Log.e(TAG, "jsonArrayDeleted: "+jsonArray );
                        notifyDataSetChanged();
                    } else {
                        jsonArray.optJSONObject(position).put("isSelected", "true");
                        holder.checkbox.setChecked(true);
                        notifyDataSetChanged();
                        Log.e(TAG, "onClick: "+"inserted" );
                        Log.e(TAG, "jsonArrayInserted: "+jsonArray );

                    }


                } catch (JSONException e) {

                }

//                if (holder.checkbox.isChecked()) {
//                    holder.checkbox.setChecked(false);
//                    int pos = holder.getAdapterPosition();
//
//                    String subjectId = jsonArray.optJSONObject(pos).optString("subject_id");
//
//                    for (int i = 0; i < selectedJsonArray.length(); i++) {
//
//                        if (subjectId.equals(selectedJsonArray.optJSONObject(i).optString("subject_id"))) {
//                            selectedJsonArray = remove(i, selectedJsonArray);
//                        }
//                    }
//
//                    Log.e(TAG, "deleteSelectedJsonArray: " + selectedJsonArray);
//
//
//                } else {
//
//                    holder.checkbox.setChecked(true);
//                    int pos = holder.getAdapterPosition();
//
//                    JSONObject jsonObject = new JSONObject();
//
//                    try {
//                        jsonObject.put("subject_id", jsonArray.optJSONObject(pos).optString("subject_id"));
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        e.getMessage();
//                    }
//
//                    selectedJsonArray.put(jsonObject);
//                    Log.e(TAG, "selectedJsonArray: " + selectedJsonArray);
//
//                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout clickSubject;
        public TextView subjectName;
        public CheckBox checkbox;
        public ViewHolder(View itemView) {
            super(itemView);
            clickSubject=itemView.findViewById(R.id.clickSubject);
            subjectName=itemView.findViewById(R.id.subjectName);
            checkbox=itemView.findViewById(R.id.checkbox);



        }
    }

    public static JSONArray remove(final int index, final JSONArray from) {
        final List<JSONObject> objs = getList(from);
        objs.remove(index);

        final JSONArray jarray = new JSONArray();
        for (final JSONObject obj : objs) {
            jarray.put(obj);
        }


        return jarray;
    }

    public static List<JSONObject> getList(final JSONArray jarray) {
        final int len = jarray.length();
        final ArrayList<JSONObject> result = new ArrayList<JSONObject>(len);
        for (int i = 0; i < len; i++) {
            final JSONObject obj = jarray.optJSONObject(i);
            if (obj != null) {
                result.add(obj);
            }
        }
        return result;
    }
}