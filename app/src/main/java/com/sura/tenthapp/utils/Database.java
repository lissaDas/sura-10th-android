package com.sura.tenthapp.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Database extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "SavedContents10th.db";
    public static final String TABLE_NAME = "SavedPagesTable";
    public static final String CHAPTER_ID = "chapter_id";
    public static final String FILE_LOCATION = "file_location";
    public static final String ID = "id";
    public static final String TIMESTAMP = "timestamp";
    public static final String STATUS = "status";
    public static final String SUBJECT_ID = "subject_id";
    public static final String SUBJECT_NAME = "suject_name";
    public static final String CHAPTER_NAME = "chapter_name";


    public Database(Context context) {
        super(context, DATABASE_NAME, null, 4);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
                + ID + " INTEGER PRIMARY KEY, "
                + SUBJECT_ID + " TEXT, "
                + CHAPTER_ID + " TEXT, "
                + SUBJECT_NAME + " TEXT, "
                + CHAPTER_NAME + " TEXT, "
                + STATUS + " TEXT, "
                + FILE_LOCATION + " TEXT, "
                + TIMESTAMP + " TEXT DEFAULT CURRENT_TIMESTAMP)";

        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }




    public void addToDatabase(String subjectId, String chapterId, String subjectName, String chapterName, String status, String fileLocation) {

        SQLiteDatabase dataBase = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Database.SUBJECT_ID, subjectId);
        values.put(Database.CHAPTER_ID, chapterId);
        values.put(Database.SUBJECT_NAME, subjectName);
        values.put(Database.CHAPTER_NAME, chapterName);
        values.put(Database.STATUS, status);
        values.put(Database.FILE_LOCATION, fileLocation);
        dataBase.insertWithOnConflict(Database.TABLE_NAME, null, values,SQLiteDatabase.CONFLICT_REPLACE);

        dataBase.close();
    }

    public JSONArray selectFromDatabase() {

        SQLiteDatabase dataBase = getWritableDatabase();

        Cursor c = dataBase.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE "
//                        + SUBJECT_NAME + " = '" + tableName + "'"
//                        + " AND "
//                        + CHAPTER_NAME + " = '" + keyName + "'"
//                        + " AND "
//                        + FILE_LOCATION + " = '" + id + "'"
//                        + " AND "
                        + STATUS + " = '" + "success" + "'"
                , null);

//        c.moveToNext();

        JSONArray jsonArray=new JSONArray();



        if (c.moveToFirst()){
            do {
                // Passing values
                JSONObject jsonObject=new JSONObject();

                String name = c.getString(c.getColumnIndex(CHAPTER_NAME));
                String name1 = c.getString(c.getColumnIndex(SUBJECT_NAME));
                String name2 = c.getString(c.getColumnIndex(FILE_LOCATION));
                String name3 = c.getString(c.getColumnIndex(CHAPTER_ID));
                String name4 = c.getString(c.getColumnIndex(ID));
                try {
                    jsonObject.put("id",name4) ;
                    jsonObject.put("chapter_name",name) ;
                    jsonObject.put("subject_division_name",name1) ;
                    jsonObject.put("file_location",name2) ;
                    jsonObject.put("chapter_id",name3) ;
                    jsonArray.put(jsonObject);



                } catch (JSONException e) {

                }

                // Do something Here with values
            } while(c.moveToNext());
        }

        String count = String.valueOf(c.getCount());

        return jsonArray;

    }

    public int checkDuplicate(String subjectId, String chapterId, String fileLocation,String status) {

        SQLiteDatabase dataBase = getReadableDatabase();

        Cursor c = dataBase.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE "
                        + SUBJECT_ID + " = '" + subjectId + "'"
                        + " AND "
                        + CHAPTER_ID + " = '" + chapterId + "'"
                        + " AND "
                        + FILE_LOCATION + " = '" + fileLocation + "'"
                        + " AND "
                        + STATUS + " = '" + status + "'"
                , null);

        c.moveToNext();

        int count = c.getCount();

        return count;


    }

    public boolean deleteTitle(String id)
    {
        SQLiteDatabase dataBase = getWritableDatabase();
        return dataBase.delete(TABLE_NAME, ID + "=" + id, null) > 0;
    }

}
