package com.sura.tenthapp.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DatabasePayment extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Payments.db";
    public static final String TABLE_NAME = "PaymentTable";
    public static final String BODY = "body";
    public static final String ID = "id";
    public static final String TIMESTAMP = "timestamp";
    public static final String STATUS = "status";


    public DatabasePayment(Context context) {
        super(context, DATABASE_NAME, null, 4);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
                + ID + " INTEGER PRIMARY KEY, "
                + STATUS + " TEXT, "
                + BODY + " TEXT, "
                + TIMESTAMP + " TEXT DEFAULT CURRENT_TIMESTAMP)";

        db.execSQL(CREATE_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }




    public long addToDatabase(String status,String body) {

        SQLiteDatabase dataBase = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(DatabasePayment.STATUS, status);
        values.put(DatabasePayment.BODY, body);

        long id=dataBase.insert(DatabasePayment.TABLE_NAME, null, values);

        dataBase.close();

        return id;
    }

    public JSONArray selectFromDatabase() {

        SQLiteDatabase dataBase = getWritableDatabase();

        Cursor c = dataBase.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE "
//                        + SUBJECT_NAME + " = '" + tableName + "'"
//                        + " AND "
//                        + CHAPTER_NAME + " = '" + keyName + "'"
//                        + " AND "
//                        + FILE_LOCATION + " = '" + id + "'"
//                        + " AND "
                        + STATUS + " = '" + "pending" + "'"
                , null);

//        c.moveToNext();

        JSONArray jsonArray=new JSONArray();

        if (c.moveToFirst()){
            do {
                // Passing values
                JSONObject jsonObject=new JSONObject();

                String name = c.getString(c.getColumnIndex(BODY));
                String name4 = c.getString(c.getColumnIndex(ID));
                try {
                    jsonObject.put("id",name4) ;
                    jsonObject.put("body",name) ;
                    jsonArray.put(jsonObject);

                } catch (JSONException e) {

                }

                // Do something Here with values
            } while(c.moveToNext());
        }

        return jsonArray;

    }

    public int checkDuplicate(String status) {

        SQLiteDatabase dataBase = getReadableDatabase();

        Cursor c = dataBase.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE "
                        + STATUS + " = '" + status + "'"
                , null);

        c.moveToNext();

        int count = c.getCount();

        return count;

    }

    public boolean deleteRow(String id)
    {
        SQLiteDatabase dataBase = getWritableDatabase();
        return dataBase.delete(TABLE_NAME, ID + "=" + id, null) > 0;
    }

}
