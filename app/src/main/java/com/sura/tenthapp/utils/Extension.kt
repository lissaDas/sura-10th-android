package com.sura.tenthapp.utils


fun Any.checkTextLengthEquals(text: String, maxLength: Int): Boolean {
    if (text != null && text.isNotBlank() && text.length == maxLength) {
        return true
    }
    return false
}

fun Any.checkTextLengthMatchesRange(text: String, maxLength: Int, minLength: Int = 1): Boolean {
    if (text != null && text.isNotBlank() && (text.length <= maxLength && text.length >= minLength)) {
        return true
    }
    return false
}

fun checkPasswordMatches(currentPassword: String, confirmPassword: String): Boolean {
    if (currentPassword.equals(confirmPassword, false)) {
        return true
    }
    return false
}

