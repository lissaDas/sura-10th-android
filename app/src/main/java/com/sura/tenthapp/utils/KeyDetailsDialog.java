package com.sura.tenthapp.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.sura.tenthapp.R;

public class KeyDetailsDialog {


    public static void showDialog(Context context) {
        final Dialog unlockChapterDialog = new Dialog(context);
        unlockChapterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        unlockChapterDialog.setContentView(R.layout.key_hints_dialog);
        unlockChapterDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        unlockChapterDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        unlockChapterDialog.getWindow().setGravity(Gravity.CENTER);
        RelativeLayout closeButton=unlockChapterDialog.findViewById(R.id.closeButton);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unlockChapterDialog.dismiss();
            }
        });
        unlockChapterDialog.show();
    }
}