package com.sura.tenthapp.utils;

import android.app.Activity;
import android.view.inputmethod.InputMethodManager;

import org.json.JSONArray;

public class SelectedSubjectArray {

     private JSONArray selectedArray;

    public JSONArray getSelectedArray() {
        return selectedArray;
    }

    public SelectedSubjectArray(JSONArray selectedArray) {
        this.selectedArray = selectedArray;
    }
}