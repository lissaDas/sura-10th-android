package com.sura.tenthapp.utils;

import android.content.Context;

public class SharedHelperModel {

    Context context;
    String string;

    public SharedHelperModel(Context context) {
        this.context=context;
    }

    public String getAccessToken(){
        string=SharedHelpers.getKey(context,"accessToken");
        return string;
    }
    public void setAccessToken(String token){
        SharedHelpers.putkey(context,"accessToken",token);
        this.string = token;
    }
    public String getOtp(){
        string=SharedHelpers.getKey(context,"otp");
        return string;
    }
    public void setOtp(String otp){
        SharedHelpers.putkey(context,"otp",otp);
        this.string = otp;
    }
    public String getNewUser(){
        string=SharedHelpers.getKey(context,"user");
        return string;
    }
    public void setNewUser(String user){
        SharedHelpers.putkey(context,"user",user);
        this.string = user;
    }
    public String getMobileNumber(){
        string=SharedHelpers.getKey(context,"mobileNumber");
        return string;
    }
    public void setMobileNumber(String mobile){
        SharedHelpers.putkey(context,"mobileNumber",mobile);
        this.string = mobile;
    }
    public String getKeysCount(){
        string=SharedHelpers.getKey(context,"keyCount");
        return string;
    }
    public void setKeysCount(String key){
        SharedHelpers.putkey(context,"keyCount",key);
        this.string = key;
    }
    public String getUserReferalCode(){
        string=SharedHelpers.getKey(context,"referalCode");
        return string;
    }
    public void setUserReferalCode(String referalCode){
        SharedHelpers.putkey(context,"referalCode",referalCode);
        this.string = referalCode;
    }
    //check if the user entered the referal code on signup
    public String getReferalCodeStatus(){
        string=SharedHelpers.getKey(context,"referalCodeStatus");
        return string;
    }
    public void setReferalCodeStatus(String referalCode){
        SharedHelpers.putkey(context,"referalCodeStatus",referalCode);
        this.string = referalCode;
    }
    //end of the referal code

    public String getDeviceToken(){
        string=SharedHelpers.getKey(context,"deviceToken");
        return string;
    }
    public void setDeviceToken(String deviceToken){
        SharedHelpers.putkey(context,"deviceToken",deviceToken);
        this.string = deviceToken;
    }

    public String getLoginStatus(){
        string=SharedHelpers.getKey(context,"loginStatus");
        return string;
    }
    public void setLoginStatus(String loginStatus){
        SharedHelpers.putkey(context,"loginStatus",loginStatus);
        this.string = loginStatus;
    }
    public String getShowCaseStatus(){
        string=SharedHelpers.getKey(context,"showCaseStatus");
        return string;
    }
    public void setShowCaseStatus(String showCaseStatus){
        SharedHelpers.putkey(context,"showCaseStatus",showCaseStatus);
        this.string = showCaseStatus;
    }
    public String getReadedMode(){
        string=SharedHelpers.getKey(context,"readedMode");
        return string;
    }
    public void setReadedMode(String readedMode){
        SharedHelpers.putkey(context,"readedMode",readedMode);
        this.string = readedMode;
    }
    public String getLang(){
        string=SharedHelpers.getKey(context,"lang");
        return string;
    }
    public void setLang(String lang){
        SharedHelpers.putkey(context,"lang",lang);
        this.string = lang;
    }
    public String getChapterListFromBreadCrumbs(){
        string=SharedHelpers.getKey(context,"getChapterOnResume");
        return string;
    }
    public void setChapterListFromBreadCrumbs(String getChapterOnResume){
        SharedHelpers.putkey(context,"getChapterOnResume",getChapterOnResume);
        this.string = getChapterOnResume;
    }
    public String getLoginStudentOrTeacher(){
        string=SharedHelpers.getKey(context,"loginStudentOrTeacher");
        return string;
    }
    public void setLoginStudentOrTeacher(String loginStudentOrTeacher){
        SharedHelpers.putkey(context,"loginStudentOrTeacher",loginStudentOrTeacher);
        this.string = loginStudentOrTeacher;
    }

    //////////////// Bottom Listener saved instance

    public String getBooleanCheckHome(){
        string=SharedHelpers.getKey(context,"booleanCheckHome");
        return string;
    }
    public void setBooleanCheckHome(String booleanCheckHome){
        SharedHelpers.putkey(context,"booleanCheckHome",booleanCheckHome);
        this.string = booleanCheckHome;
    }
    public String getHomeStudyContent(){
        string=SharedHelpers.getKey(context,"homeStudyContent");
        return string;
    }
    public void setHomeStudyContent(String homeStudyContent){
        SharedHelpers.putkey(context,"homeStudyContent",homeStudyContent);
        this.string = homeStudyContent;
    }

    public String getHomeAds(){
        string=SharedHelpers.getKey(context,"homeAds");
        return string;
    }
    public void setHomeAds(String homeAds){
        SharedHelpers.putkey(context,"homeAds",homeAds);
        this.string = homeAds;
    }


    public String getBooleanCheckFiles(){
        string=SharedHelpers.getKey(context,"booleanCheckFiles");
        return string;
    }
    public void setBooleanCheckFiles(String booleanCheckFiles){
        SharedHelpers.putkey(context,"booleanCheckFiles",booleanCheckFiles);
        this.string = booleanCheckFiles;
    }
    public String getFilesContent(){
        string=SharedHelpers.getKey(context,"filesContent");
        return string;
    }
    public void setFilesContent(String filesContent){
        SharedHelpers.putkey(context,"filesContent",filesContent);
        this.string = filesContent;
    }

    //Continue Study Array

    public String getContinueStudyArray(){
        string=SharedHelpers.getKey(context,"continueStudyArray");
        return string;
    }
    public void setContinueStudyArray(String continueStudyArray){
        SharedHelpers.putkey(context,"continueStudyArray",continueStudyArray);
        this.string = continueStudyArray;
    }

    //IS OFFER APPLIED
    public String getIsOfferApplied(){
        string=SharedHelpers.getKey(context,"isOfferApplied");
        return string;
    }
    public void setIsOfferApplied(String isOfferApplied){
        SharedHelpers.putkey(context,"isOfferApplied",isOfferApplied);
        this.string = isOfferApplied;
    }
}