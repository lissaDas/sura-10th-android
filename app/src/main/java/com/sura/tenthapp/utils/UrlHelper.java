package com.sura.tenthapp.utils;

import android.os.Build;

import com.sura.tenthapp.BuildConfig;

public class UrlHelper {

//    public static String BASEURL= BuildConfig.APP_BASEURL; //LIVE
//    public static String BASEURL="http://suracurrentaffairs.com/sura_10/public/api/"; //LIVE
//    public static String BASEURL="http://188.166.228.50/sura/public/api/";   //RESEARCH
    public static String BASEURL="http://167.71.225.243/sura/public/api/";

    public static String getHomeChapter=BASEURL+"subject_v9";
    public static String getChapterList=BASEURL+"chapter_v9";
    public static String getOtp=BASEURL+"otp_version_v7";
    public static String login=BASEURL+"login_v8";
    public static String singup=BASEURL+"signup_v8";
    public static String setMedium=BASEURL+"setmedium";
    public static String getChapterContent=BASEURL+"chapter_content_v9";
//    public static String getFiles=BASEURL+"pdf_list";
    public static String getFiles=BASEURL+"pdf_list_v8";
    public static String getFavourite=BASEURL+"list_favourites";
    public static String getDoubts=BASEURL+"list_doubts";
    public static String addDoubts=BASEURL+"add_doubt";
//    public static String getQuestion=BASEURL+"question";
    public static String getQuestion=BASEURL+"question_v8";
    public static String getProfile=BASEURL+"view_profile_v8";
    public static String postProfile=BASEURL+"edit_profile_v8";
    public static String postFeedback=BASEURL+"add_feedback";
    public static String postAddFav=BASEURL+"add_favourites";
    public static String postRemoveFav=BASEURL+"remove_favourites";
    public static String postReferalCode=BASEURL+"referral_code";
    public static String postUnlockChapter=BASEURL+"unlock_chapter";
    public static String getAds=BASEURL+"ads_v9";
    public static String postSearch=BASEURL+"search";
    public static String postCheckFav=BASEURL+"favourites_check";
    public static String postPdfLog=BASEURL+"pdf_log";
    public static String getPrivacyPolicy=BASEURL+"privacy_policy";
    public static String getTermsConditions=BASEURL+"app_terms";
    public static String getDisclaimer=BASEURL+"disclaimer";
    public static String getRefundPolicy=BASEURL+"refund";
    public static String getContactUs=BASEURL+"contact_us";
    public static String getAboutUs=BASEURL+"sura_doc";
    public static String postPaymentAck=BASEURL+"payment_acknowledgement_v7";
    public static String postSubjectAmount=BASEURL+"subject_amount_new";
    public static String postUserInvoice=BASEURL+"user_invoice";
    public static String getDoubtSubject=BASEURL+"doubt_subject_v8";
    public static String getAnswersSubject=BASEURL+"answer_user_v8";
    public static String postAnswer=BASEURL+"question_answer_v8";
    public static String postLikeDislike=BASEURL+"ups_downs_v8";
    public static String getAllSubjects=BASEURL+"subject_all_v8";
    public static String getTeacherDoubtSubjects=BASEURL+"teacher_doubts_v8";
    public static String districts_10=BASEURL+"districts_10";
    public static String storelist_10=BASEURL+"storelist_10";
    public static String list_notification=BASEURL+"list_notification";
    public static String list_notification_v2=BASEURL+"list_notification_v2";
    public static String notification_detail=BASEURL+"list_notification_v2";
    public static String notification_detail_v2=BASEURL+"notification_detail_v2";
    public static String logout=BASEURL+"logout";
    public static String checkServer="http://188.166.228.50/sura/public/api/sura_maintanace";


}