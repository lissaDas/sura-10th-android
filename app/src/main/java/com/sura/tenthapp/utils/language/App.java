package com.sura.tenthapp.utils.language;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.util.Log;

import com.sura.tenthapp.utils.SharedHelperModel;

import java.util.Locale;

public class App extends Application {

    Context context;
    public void onCreate(){
        super.onCreate();

        SharedHelperModel sharedHelper=new SharedHelperModel(context);
        String language = sharedHelper.getLang();
        Log.e("Language", "onCreate: "+language);
        LocaleUtils.setLocale(new Locale(language));
        LocaleUtils.updateConfig(this, getBaseContext().getResources().getConfiguration());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleUtils.updateConfig(this, newConfig);
    }

//    @Override
//    protected void attachBaseContext(Context base) {
//        MultiDex.install(this);
//    }
}