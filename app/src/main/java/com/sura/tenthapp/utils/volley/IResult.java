package com.sura.tenthapp.utils.volley;

import com.android.volley.VolleyError;

import org.json.JSONObject;

public interface IResult {
    void notifySuccess(String requestType, JSONObject response);
    void notifySuccessString(String requestType, String response);
    void notifyError(String requestType, String error);
} 