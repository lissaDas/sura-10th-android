package com.sura.tenthapp.utils.volley;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sura.tenthapp.modules.login_register.MobileNumberLogin;
import com.sura.tenthapp.utils.SharedHelperModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VolleyService {

    IResult mResultCallback = null;
    Context mContext;

    public VolleyService(IResult resultCallback, Context context){
        mResultCallback = resultCallback;
        mContext = context;
    }


    public void postDataStringVolley(final String requestType, final String url, final HashMap<String, String> sendObj, final HashMap<String, String> header){

        try {
            RequestQueue queue = Volley.newRequestQueue(mContext);

            StringRequest jsonArrayRequest=new StringRequest(Request.Method.POST,url,new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if(mResultCallback != null)
                        mResultCallback.notifySuccessString(requestType,response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if(mResultCallback != null){
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            mResultCallback.notifyError(requestType, "No Internet Connection.. Please Try Again..");
                        } else if (error instanceof AuthFailureError) {
                            mResultCallback.notifyError(requestType, "Authentication Failure Error.. Please Try Again..");
                        } else if (error instanceof ServerError) {
                            mResultCallback.notifyError(requestType, "Server Error.. Please Try Again..");
                        } else if (error instanceof NetworkError) {
                            mResultCallback.notifyError(requestType, "Network Error.. Please Try Again..");
                        } else if (error instanceof ParseError) {
                            mResultCallback.notifyError(requestType, "Parse Error.. Please Try Again..");
                            SharedHelperModel sharedHelperModel = new SharedHelperModel(mContext);
                            sharedHelperModel.setLoginStatus("0");
                            sharedHelperModel.setLang("");
                            JSONArray jsonArray=new JSONArray();
                            sharedHelperModel.setContinueStudyArray(jsonArray.toString());
                            sharedHelperModel.setBooleanCheckHome("0");
                            sharedHelperModel.setBooleanCheckFiles("0");
                            Intent intent = new Intent(mContext, MobileNumberLogin.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            mContext.startActivity(intent);
                            Toast.makeText(mContext, "Please Re-Login", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            })

            {
                @Override
                protected Map<String,String> getParams(){

                    return sendObj;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    return header;
                }
            };
            queue.add(jsonArrayRequest);

        }catch(Exception e){

            Log.e("Volley", "getDataVolley: "+e.getMessage() );

        }
    }



    public void postDataVolley(final String requestType, String url, final HashMap<String, String> sendObj, final HashMap<String, String> header){

        try {
            RequestQueue queue = Volley.newRequestQueue(mContext);
            
            JSONObject jsonObject = new JSONObject(sendObj);



            JsonObjectRequest jsonArrayRequest=new JsonObjectRequest(Request.Method.POST,url,jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if(mResultCallback != null)
                        mResultCallback.notifySuccess(requestType,response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if(mResultCallback != null){
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            mResultCallback.notifyError(requestType, "No Internet Connection.. Please Try Again..");
                        } else if (error instanceof AuthFailureError) {
                            mResultCallback.notifyError(requestType, "Authentication Failure Error.. Please Try Again..");
                        } else if (error instanceof ServerError) {
                            mResultCallback.notifyError(requestType, "Server Error.. Please Try Again..");
                        } else if (error instanceof NetworkError) {
                            mResultCallback.notifyError(requestType, "Network Error.. Please Try Again..");
                        } else if (error instanceof ParseError) {
                            mResultCallback.notifyError(requestType, "Parse Error.. Please Try Again..");
                            SharedHelperModel sharedHelperModel = new SharedHelperModel(mContext);
                            sharedHelperModel.setLoginStatus("0");
                            sharedHelperModel.setLang("");
                            JSONArray jsonArray=new JSONArray();
                            sharedHelperModel.setContinueStudyArray(jsonArray.toString());
                            sharedHelperModel.setBooleanCheckHome("0");
                            sharedHelperModel.setBooleanCheckFiles("0");
                            Intent intent = new Intent(mContext, MobileNumberLogin.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            mContext.startActivity(intent);
                            Toast.makeText(mContext, "Please Re-Login", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {

                    return header;
                }
            } ;
            jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(jsonArrayRequest);

        }catch(Exception e){
            Log.e("Volley", "getDataVolley: "+e.getMessage() );
        }
    }

    public void getDataVolley(final String requestType, String url, final HashMap<String, String> mapParams){
        final SharedHelperModel sharedHelperModel=new SharedHelperModel(mContext);
        try {
            RequestQueue queue = Volley.newRequestQueue(mContext);

            JsonObjectRequest jsonArrayRequest=new JsonObjectRequest(Request.Method.GET,url,null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if(mResultCallback != null)
                        mResultCallback.notifySuccess(requestType,response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        mResultCallback.notifyError(requestType, "No Internet Connection.. Please Try Again..");
                    } else if (error instanceof AuthFailureError) {
                        mResultCallback.notifyError(requestType, "Authentication Failure Error.. Please Try Again..");
                    } else if (error instanceof ServerError) {
                        mResultCallback.notifyError(requestType, "Server Error.. Please Try Again..");
                    } else if (error instanceof NetworkError) {
                        mResultCallback.notifyError(requestType, "Network Error.. Please Try Again..");
                    } else if (error instanceof ParseError) {
                        mResultCallback.notifyError(requestType, "Parse Error.. Please Try Again..");
                        SharedHelperModel sharedHelperModel = new SharedHelperModel(mContext);
                        sharedHelperModel.setLoginStatus("0");
                        sharedHelperModel.setLang("");
                        JSONArray jsonArray=new JSONArray();
                        sharedHelperModel.setContinueStudyArray(jsonArray.toString());
                        sharedHelperModel.setBooleanCheckHome("0");
                        sharedHelperModel.setBooleanCheckFiles("0");
                        Intent intent = new Intent(mContext, MobileNumberLogin.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        mContext.startActivity(intent);
                        Toast.makeText(mContext, "Please Re-Login", Toast.LENGTH_SHORT).show();
                    }
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {


                    return mapParams;
                }
            };


            queue.add(jsonArrayRequest);

        }catch(Exception e){

            e.printStackTrace();

            Log.e("Volley", "getDataVolley: "+e.getMessage() );

        }
    }

    public void getStringDataVolley(final String requestType, String url, final HashMap<String, String> mapParams){
        try {
            RequestQueue queue = Volley.newRequestQueue(mContext);

            StringRequest jsonArrayRequest=new StringRequest(Request.Method.GET,url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if(mResultCallback != null)
                        mResultCallback.notifySuccessString(requestType,response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        mResultCallback.notifyError(requestType, "No Internet Connection.. Please Try Again..");
                    } else if (error instanceof AuthFailureError) {
                        mResultCallback.notifyError(requestType, "Authentication Failure Error.. Please Try Again..");
                    } else if (error instanceof ServerError) {
                        mResultCallback.notifyError(requestType, "Server Error.. Please Try Again..");
                    } else if (error instanceof NetworkError) {
                        mResultCallback.notifyError(requestType, "Network Error.. Please Try Again..");
                    } else if (error instanceof ParseError) {
                        mResultCallback.notifyError(requestType, "Parse Error.. Please Try Again..");
                        SharedHelperModel sharedHelperModel = new SharedHelperModel(mContext);
                        sharedHelperModel.setLoginStatus("0");
                        sharedHelperModel.setLang("");
                        JSONArray jsonArray=new JSONArray();
                        sharedHelperModel.setContinueStudyArray(jsonArray.toString());
                        sharedHelperModel.setBooleanCheckHome("0");
                        sharedHelperModel.setBooleanCheckFiles("0");
                        Intent intent = new Intent(mContext, MobileNumberLogin.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        mContext.startActivity(intent);
                        Toast.makeText(mContext, "Please Re-Login", Toast.LENGTH_SHORT).show();
                    }
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {


                    return mapParams;
                }
            };
            jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(jsonArrayRequest);

        }catch(Exception e){

            e.printStackTrace();

            Log.e("Volley", "getDataVolley: "+e.getMessage() );

        }
    }

}